package com.guidewire.jflightnova.web;

import com.guidewire.jflightnova.web.api.model.ClientDecConfig;
import com.guidewire.jflightnova.web.api.ClientDecConfigsApiController;
import com.guidewire.jflightnova.web.api.ClientDecConfigsApiDelegate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.guidewire.jflightnova.web.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Disabled
public class ClientDecConfigsApiControllerIT extends AbstractEntityApiControllerIT {

    private static final String DEFAULT_CBSCLIENTNAME = "AAAAAAAAAA";
    private static final String UPDATED_CBSCLIENTNAME = "BBBBBBBBBB";
    private static final String DEFAULT_ENVIRONMENT_LEVEL = "AAAAAAAAAA";
    private static final String UPDATED_ENVIRONMENT_LEVEL = "BBBBBBBBBB";
    private static final String DEFAULT_DB_USER = "AAAAAAAAAA";
    private static final String UPDATED_DB_USER = "BBBBBBBBBB";

    @Autowired
    private ClientDecConfigsApiDelegate clientDecConfigsApiDelegate;

    private MockMvc restClientDecConfigMockMvc;

    private ClientDecConfig clientDecConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientDecConfigsApiController clientDecConfigsApiController = new ClientDecConfigsApiController(clientDecConfigsApiDelegate);
        this.restClientDecConfigMockMvc = MockMvcBuilders.standaloneSetup(clientDecConfigsApiController)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .build();
    }

    public static ClientDecConfig createEntity() {
        ClientDecConfig clientDecConfig = new ClientDecConfig()
            .cbsclientname(DEFAULT_CBSCLIENTNAME)
            .environmentLevel(DEFAULT_ENVIRONMENT_LEVEL)
            .dbUser(DEFAULT_DB_USER);
        return clientDecConfig;
    }

    @BeforeEach
    public void initTest() {
        clientDecConfig = createEntity();
    }

    @Test
    @Transactional
    public void createClientDecConfig() throws Exception {
        // Create the ClientDecConfig
        restClientDecConfigMockMvc.perform(post("/api/v1/client-dec-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDecConfig)))
            .andExpect(status().isCreated());
        // Validate the ClientDecConfig in the database
        List<ClientDecConfig> clientDecConfigList = clientDecConfigsApiDelegate.getAllClientDecConfigs().getBody();
        assertThat(clientDecConfigList).hasSize(1);
        ClientDecConfig testClientDecConfig = clientDecConfigList.get(0);
        assertThat(testClientDecConfig.getCbsclientname()).isEqualTo(DEFAULT_CBSCLIENTNAME);
        assertThat(testClientDecConfig.getEnvironmentLevel()).isEqualTo(DEFAULT_ENVIRONMENT_LEVEL);
        assertThat(testClientDecConfig.getDbUser()).isEqualTo(DEFAULT_DB_USER);
        clientDecConfigsApiDelegate.deleteClientDecConfig(testClientDecConfig.getId());
    }

    @Test
    @Transactional
    public void createClientDecConfigWithExistingId() throws Exception {
        // Create the ClientDecConfig with an existing ID
        clientDecConfig.setId(1L);
        // An entity with an existing ID cannot be created, so this API call must fail
        restClientDecConfigMockMvc.perform(post("/api/v1/client-dec-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDecConfig)))
            .andExpect(status().isBadRequest());
        // Validate the ClientDecConfig in the database
        List<ClientDecConfig> clientDecConfigList = clientDecConfigsApiDelegate.getAllClientDecConfigs().getBody();
        assertThat(clientDecConfigList).hasSize(0);
    }

    @Test
    @Transactional
    public void getAllClientDecConfigs() throws Exception {
        // Initialize the database
        ClientDecConfig createdClientDecConfig = clientDecConfigsApiDelegate.createClientDecConfig(clientDecConfig).getBody();
        // Get all the clientDecConfigList
        restClientDecConfigMockMvc.perform(get("/api/v1/client-dec-configs"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(createdClientDecConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].cbsclientname").value(hasItem(DEFAULT_CBSCLIENTNAME)))
            .andExpect(jsonPath("$.[*].environmentLevel").value(hasItem(DEFAULT_ENVIRONMENT_LEVEL)))
            .andExpect(jsonPath("$.[*].dbUser").value(hasItem(DEFAULT_DB_USER)));
        clientDecConfigsApiDelegate.deleteClientDecConfig(createdClientDecConfig.getId());
    }

    @Test
    @Transactional
    public void getClientDecConfig() throws Exception {
        // Initialize the database
        ClientDecConfig createdClientDecConfig = clientDecConfigsApiDelegate.createClientDecConfig(clientDecConfig).getBody();
        // Get the clientDecConfig
        restClientDecConfigMockMvc.perform(get("/api/v1/client-dec-configs/{id}", createdClientDecConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(createdClientDecConfig.getId().intValue()))
            .andExpect(jsonPath("$.cbsclientname").value(DEFAULT_CBSCLIENTNAME))
            .andExpect(jsonPath("$.environmentLevel").value(DEFAULT_ENVIRONMENT_LEVEL))
            .andExpect(jsonPath("$.dbUser").value(DEFAULT_DB_USER));
        clientDecConfigsApiDelegate.deleteClientDecConfig(createdClientDecConfig.getId());
    }

    @Test
    @Transactional
    public void getNonExistingClientDecConfig() throws Exception {
        // Get the clientDecConfig
        restClientDecConfigMockMvc.perform(get("/api/v1/client-dec-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }


    @Test
    @Transactional
    public void updateClientDecConfig() throws Exception {
        // Initialize the database
        ClientDecConfig createdClientDecConfig = clientDecConfigsApiDelegate.createClientDecConfig(clientDecConfig).getBody();

        // Update the clientDecConfig
        ClientDecConfig updatedClientDecConfig = clientDecConfigsApiDelegate.getClientDecConfig(createdClientDecConfig.getId()).getBody();
        updatedClientDecConfig
            .cbsclientname(UPDATED_CBSCLIENTNAME)
            .environmentLevel(UPDATED_ENVIRONMENT_LEVEL)
            .dbUser(UPDATED_DB_USER);

        restClientDecConfigMockMvc.perform(put("/api/v1/client-dec-configs/{id}", createdClientDecConfig.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientDecConfig)))
            .andExpect(status().isOk());

        // Validate the ClientDecConfig in the database
        List<ClientDecConfig> clientDecConfigList = clientDecConfigsApiDelegate.getAllClientDecConfigs().getBody();
        assertThat(clientDecConfigList).hasSize(1);
        ClientDecConfig testClientDecConfig = clientDecConfigList.get(0);
        assertThat(testClientDecConfig.getCbsclientname()).isEqualTo(UPDATED_CBSCLIENTNAME);
        assertThat(testClientDecConfig.getEnvironmentLevel()).isEqualTo(UPDATED_ENVIRONMENT_LEVEL);
        assertThat(testClientDecConfig.getDbUser()).isEqualTo(UPDATED_DB_USER);
        clientDecConfigsApiDelegate.deleteClientDecConfig(testClientDecConfig.getId());
    }

    @Test
    @Transactional
    public void updateNonExistingClientDecConfig() throws Exception {
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientDecConfigMockMvc.perform(put("/api/v1/client-dec-configs/{id}", Long.MAX_VALUE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDecConfig)))
            .andExpect(status().isNotFound());

        // Validate the ClientDecConfig in the database
        List<ClientDecConfig> clientDecConfigList = clientDecConfigsApiDelegate.getAllClientDecConfigs().getBody();
        assertThat(clientDecConfigList).hasSize(0);
    }

    @Test
    @Transactional
    public void deleteClientDecConfig() throws Exception {
        // Initialize the database
        ClientDecConfig createdClientDecConfig = clientDecConfigsApiDelegate.createClientDecConfig(clientDecConfig).getBody();

        // Delete the clientDecConfig
        restClientDecConfigMockMvc.perform(delete("/api/v1/client-dec-configs/{id}", createdClientDecConfig.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClientDecConfig> clientDecConfigList = clientDecConfigsApiDelegate.getAllClientDecConfigs().getBody();
        assertThat(clientDecConfigList).hasSize(0);
    }

    @Test
    @Transactional
    public void deleteNonExistingClientDecConfig() throws Exception {
        // Delete the clientDecConfig
        restClientDecConfigMockMvc.perform(delete("/api/v1/client-dec-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
