package com.guidewire.jflightnova.web;

import com.guidewire.jflightnova.JflightnovaApp;
import com.guidewire.jflightnova.config.TestSecurityConfiguration;
import com.guidewire.jflightnova.web.errors.ExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = {JflightnovaApp.class, TestSecurityConfiguration.class})
@ActiveProfiles("test")
abstract public class AbstractEntityApiControllerIT {
    @Autowired
    protected MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    protected ExceptionTranslator exceptionTranslator;
}
