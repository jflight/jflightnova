package com.guidewire.jflightnova.web;

import com.guidewire.jflightnova.web.api.model.Car;
import com.guidewire.jflightnova.web.api.CarsApiController;
import com.guidewire.jflightnova.web.api.CarsApiDelegate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.guidewire.jflightnova.web.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CarsApiControllerIT extends AbstractEntityApiControllerIT {

    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_COLOR = "BBBBBBBBBB";
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CarsApiDelegate carsApiDelegate;

    private MockMvc restCarMockMvc;

    private Car car;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CarsApiController carsApiController = new CarsApiController(carsApiDelegate);
        this.restCarMockMvc = MockMvcBuilders.standaloneSetup(carsApiController)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .build();
    }

    public static Car createEntity() {
        Car car = new Car()
            .color(DEFAULT_COLOR)
            .name(DEFAULT_NAME);
        return car;
    }

    @BeforeEach
    public void initTest() {
        car = createEntity();
    }

    @Test
    @Transactional
    public void createCar() throws Exception {
        // Create the Car
        restCarMockMvc.perform(post("/api/v1/cars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(car)))
            .andExpect(status().isCreated());
        // Validate the Car in the database
        List<Car> carList = carsApiDelegate.getAllCars().getBody();
        assertThat(carList).hasSize(1);
        Car testCar = carList.get(0);
        assertThat(testCar.getColor()).isEqualTo(DEFAULT_COLOR);
        assertThat(testCar.getName()).isEqualTo(DEFAULT_NAME);
        carsApiDelegate.deleteCar(testCar.getId());
    }

    @Test
    @Transactional
    public void createCarWithExistingId() throws Exception {
        // Create the Car with an existing ID
        car.setId(1L);
        // An entity with an existing ID cannot be created, so this API call must fail
        restCarMockMvc.perform(post("/api/v1/cars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(car)))
            .andExpect(status().isBadRequest());
        // Validate the Car in the database
        List<Car> carList = carsApiDelegate.getAllCars().getBody();
        assertThat(carList).hasSize(0);
    }

    @Test
    @Transactional
    public void getAllCars() throws Exception {
        // Initialize the database
        Car createdCar = carsApiDelegate.createCar(car).getBody();
        // Get all the carList
        restCarMockMvc.perform(get("/api/v1/cars"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(createdCar.getId().intValue())))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
        carsApiDelegate.deleteCar(createdCar.getId());
    }

    @Test
    @Transactional
    public void getCar() throws Exception {
        // Initialize the database
        Car createdCar = carsApiDelegate.createCar(car).getBody();
        // Get the car
        restCarMockMvc.perform(get("/api/v1/cars/{id}", createdCar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(createdCar.getId().intValue()))
            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
        carsApiDelegate.deleteCar(createdCar.getId());
    }

    @Test
    @Transactional
    public void getNonExistingCar() throws Exception {
        // Get the car
        restCarMockMvc.perform(get("/api/v1/cars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }


    @Test
    @Transactional
    public void updateCar() throws Exception {
        // Initialize the database
        Car createdCar = carsApiDelegate.createCar(car).getBody();

        // Update the car
        Car updatedCar = carsApiDelegate.getCar(createdCar.getId()).getBody();
        updatedCar
            .color(UPDATED_COLOR)
            .name(UPDATED_NAME);

        restCarMockMvc.perform(put("/api/v1/cars/{id}", createdCar.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCar)))
            .andExpect(status().isOk());

        // Validate the Car in the database
        List<Car> carList = carsApiDelegate.getAllCars().getBody();
        assertThat(carList).hasSize(1);
        Car testCar = carList.get(0);
        assertThat(testCar.getColor()).isEqualTo(UPDATED_COLOR);
        assertThat(testCar.getName()).isEqualTo(UPDATED_NAME);
        carsApiDelegate.deleteCar(testCar.getId());
    }

    @Test
    @Transactional
    public void updateNonExistingCar() throws Exception {
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCarMockMvc.perform(put("/api/v1/cars/{id}", Long.MAX_VALUE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(car)))
            .andExpect(status().isNotFound());

        // Validate the Car in the database
        List<Car> carList = carsApiDelegate.getAllCars().getBody();
        assertThat(carList).hasSize(0);
    }

    @Test
    @Transactional
    public void deleteCar() throws Exception {
        // Initialize the database
        Car createdCar = carsApiDelegate.createCar(car).getBody();

        // Delete the car
        restCarMockMvc.perform(delete("/api/v1/cars/{id}", createdCar.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Car> carList = carsApiDelegate.getAllCars().getBody();
        assertThat(carList).hasSize(0);
    }

    @Test
    @Transactional
    public void deleteNonExistingCar() throws Exception {
        // Delete the car
        restCarMockMvc.perform(delete("/api/v1/cars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
