package com.guidewire.jflightnova.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.guidewire.jflightnova.web.TestUtil;

public class ClientDecConfigTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientDecConfig.class);
        ClientDecConfig clientDecConfig1 = new ClientDecConfig();
        clientDecConfig1.setId(1L);
        ClientDecConfig clientDecConfig2 = new ClientDecConfig();
        clientDecConfig2.setId(clientDecConfig1.getId());
        assertThat(clientDecConfig1).isEqualTo(clientDecConfig2);
        clientDecConfig2.setId(2L);
        assertThat(clientDecConfig1).isNotEqualTo(clientDecConfig2);
        clientDecConfig1.setId(null);
        assertThat(clientDecConfig1).isNotEqualTo(clientDecConfig2);
    }
}
