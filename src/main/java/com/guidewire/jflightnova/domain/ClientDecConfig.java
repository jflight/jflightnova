package com.guidewire.jflightnova.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A ClientDecConfig.
 */
@Entity
@Table(name = "client_dec_config")
public class ClientDecConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cbsclientname")
    private String cbsclientname;

    @Column(name = "environment_level")
    private String environmentLevel;

    @Column(name = "db_user")
    private String dbUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCbsclientname() {
        return cbsclientname;
    }

    public ClientDecConfig cbsclientname(String cbsclientname) {
        this.cbsclientname = cbsclientname;
        return this;
    }

    public void setCbsclientname(String cbsclientname) {
        this.cbsclientname = cbsclientname;
    }

    public String getEnvironmentLevel() {
        return environmentLevel;
    }

    public ClientDecConfig environmentLevel(String environmentLevel) {
        this.environmentLevel = environmentLevel;
        return this;
    }

    public void setEnvironmentLevel(String environmentLevel) {
        this.environmentLevel = environmentLevel;
    }

    public String getDbUser() {
        return dbUser;
    }

    public ClientDecConfig dbUser(String dbUser) {
        this.dbUser = dbUser;
        return this;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientDecConfig)) {
            return false;
        }
        return id != null && id.equals(((ClientDecConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientDecConfig{" +
            "id=" + getId() +
            ", cbsclientname='" + getCbsclientname() + "'" +
            ", environmentLevel='" + getEnvironmentLevel() + "'" +
            ", dbUser='" + getDbUser() + "'" +
            "}";
    }
}
