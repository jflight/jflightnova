package com.guidewire.jflightnova.repository;

import com.guidewire.jflightnova.client.helios.api.SpringClientApiClient;
import com.guidewire.jflightnova.client.helios.model.Environment;
import com.guidewire.jflightnova.domain.ClientDecConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;


@Repository
public class ClientDecConfigRepositoryImpl implements ClientDecConfigRepository {

    private SpringClientApiClient springClientApiClient;

    @Autowired
    ClientDecConfigRepositoryImpl(SpringClientApiClient springClientApiClient) {
        this.springClientApiClient = springClientApiClient;
    }

    @Override
    public List<ClientDecConfig> findAll() {
        ClientDecConfig clientDecConfig = new ClientDecConfig();
        clientDecConfig.setId(1L);
        clientDecConfig.setCbsclientname("Aviva");
        clientDecConfig.setDbUser("liveuser");
        clientDecConfig.setEnvironmentLevel("dev");
        System.out.println(springClientApiClient);

        ResponseEntity<Environment> springConfig = springClientApiClient.getSpringConfig("jflightnova", "aviva_dev", null, null, null);
        System.out.println("aaaaaaaaa " + springConfig.getStatusCodeValue());

        return new ArrayList<>(Arrays.asList(clientDecConfig));


    }

    @Override
    public List<ClientDecConfig> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<ClientDecConfig> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public <S extends ClientDecConfig> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ClientDecConfig> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<ClientDecConfig> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ClientDecConfig getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends ClientDecConfig> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ClientDecConfig> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Page<ClientDecConfig> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ClientDecConfig> S save(S entity) {
        return null;
    }

    @Override
    public Optional<ClientDecConfig> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ClientDecConfig entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends ClientDecConfig> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ClientDecConfig> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ClientDecConfig> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ClientDecConfig> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ClientDecConfig> boolean exists(Example<S> example) {
        return false;
    }
}

