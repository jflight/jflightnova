package com.guidewire.jflightnova.repository;

import com.guidewire.jflightnova.domain.ClientDecConfig;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ClientDecConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientDecConfigRepository extends JpaRepository<ClientDecConfig, Long> {
}
