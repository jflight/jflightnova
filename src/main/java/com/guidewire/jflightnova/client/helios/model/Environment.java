package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.PropertySource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Environment
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class Environment   {
  @JsonProperty("label")
  private String label = "master";

  @JsonProperty("name")
  private String name;

  @JsonProperty("profiles")
  @Valid
  private List<String> profiles = null;

  @JsonProperty("propertySources")
  @Valid
  private List<PropertySource> propertySources = null;

  @JsonProperty("state")
  private String state;

  @JsonProperty("version")
  private String version;

  public Environment label(String label) {
    this.label = label;
    return this;
  }

  /**
   * Get label
   * @return label
  */
  @ApiModelProperty(readOnly = true, value = "")


  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Environment name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  */
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Environment profiles(List<String> profiles) {
    this.profiles = profiles;
    return this;
  }

  public Environment addProfilesItem(String profilesItem) {
    if (this.profiles == null) {
      this.profiles = new ArrayList<>();
    }
    this.profiles.add(profilesItem);
    return this;
  }

  /**
   * Get profiles
   * @return profiles
  */
  @ApiModelProperty(value = "")


  public List<String> getProfiles() {
    return profiles;
  }

  public void setProfiles(List<String> profiles) {
    this.profiles = profiles;
  }

  public Environment propertySources(List<PropertySource> propertySources) {
    this.propertySources = propertySources;
    return this;
  }

  public Environment addPropertySourcesItem(PropertySource propertySourcesItem) {
    if (this.propertySources == null) {
      this.propertySources = new ArrayList<>();
    }
    this.propertySources.add(propertySourcesItem);
    return this;
  }

  /**
   * Get propertySources
   * @return propertySources
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<PropertySource> getPropertySources() {
    return propertySources;
  }

  public void setPropertySources(List<PropertySource> propertySources) {
    this.propertySources = propertySources;
  }

  public Environment state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  */
  @ApiModelProperty(value = "")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Environment version(String version) {
    this.version = version;
    return this;
  }

  /**
   * Get version
   * @return version
  */
  @ApiModelProperty(value = "")


  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Environment environment = (Environment) o;
    return Objects.equals(this.label, environment.label) &&
        Objects.equals(this.name, environment.name) &&
        Objects.equals(this.profiles, environment.profiles) &&
        Objects.equals(this.propertySources, environment.propertySources) &&
        Objects.equals(this.state, environment.state) &&
        Objects.equals(this.version, environment.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, name, profiles, propertySources, state, version);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Environment {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    profiles: ").append(toIndentedString(profiles)).append("\n");
    sb.append("    propertySources: ").append(toIndentedString(propertySources)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

