package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApplyToLayerProjectEnvironmentAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class ApplyToLayerProjectEnvironmentAllOf   {
  @JsonProperty("project")
  private String project;

  @JsonProperty("environmentLevel")
  private String environmentLevel;

  public ApplyToLayerProjectEnvironmentAllOf project(String project) {
    this.project = project;
    return this;
  }

  /**
   *  Project to apply settings
   * @return project
  */
  @ApiModelProperty(required = true, value = " Project to apply settings")
  @NotNull


  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public ApplyToLayerProjectEnvironmentAllOf environmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
    return this;
  }

  /**
   * Environment level for the setting values. Examples: dev, staging, uat, prod, etc..
   * @return environmentLevel
  */
  @ApiModelProperty(required = true, value = "Environment level for the setting values. Examples: dev, staging, uat, prod, etc..")
  @NotNull


  public String getEnvironmentLevel() {
    return environmentLevel;
  }

  public void setEnvironmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplyToLayerProjectEnvironmentAllOf applyToLayerProjectEnvironmentAllOf = (ApplyToLayerProjectEnvironmentAllOf) o;
    return Objects.equals(this.project, applyToLayerProjectEnvironmentAllOf.project) &&
        Objects.equals(this.environmentLevel, applyToLayerProjectEnvironmentAllOf.environmentLevel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(project, environmentLevel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplyToLayerProjectEnvironmentAllOf {\n");
    
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    environmentLevel: ").append(toIndentedString(environmentLevel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

