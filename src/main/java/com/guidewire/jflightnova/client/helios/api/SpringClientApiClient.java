package com.guidewire.jflightnova.client.helios.api;

import org.springframework.cloud.openfeign.FeignClient;
import com.guidewire.jflightnova.client.helios.ClientConfiguration;

@FeignClient(name="${springClient.name:springClient}", url="${springClient.url:https://helios.api.us-east-2.service.guidewire.net/api/v1}", configuration = ClientConfiguration.class)
public interface SpringClientApiClient extends SpringClientApi {
}
