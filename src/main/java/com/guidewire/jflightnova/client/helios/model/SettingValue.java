package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import com.guidewire.jflightnova.client.helios.model.SettingValueApplyTo;
import com.guidewire.jflightnova.client.helios.model.SettingValueBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * contains the value for a given setting for a given application and deployment (environment or a layer of an environment)
 */
@ApiModel(description = "contains the value for a given setting for a given application and deployment (environment or a layer of an environment)")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingValue   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("applicationId")
  private Long applicationId;

  @JsonProperty("name")
  private String name;

  @JsonProperty("value")
  private String value;

  @JsonProperty("applyTo")
  private SettingValueApplyTo applyTo;

  @JsonProperty("createdBy")
  private String createdBy;

  @JsonProperty("createdOn")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime createdOn;

  @JsonProperty("modifiedBy")
  private String modifiedBy;

  @JsonProperty("modifiedOn")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime modifiedOn;

  public SettingValue id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SettingValue version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public SettingValue applicationId(Long applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * The primary key of the application this entity is associated with.
   * @return applicationId
  */
  @ApiModelProperty(required = true, value = "The primary key of the application this entity is associated with.")
  @NotNull


  public Long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(Long applicationId) {
    this.applicationId = applicationId;
  }

  public SettingValue name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the name of the setting.
   * @return name
  */
  @ApiModelProperty(required = true, value = "the name of the setting.")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SettingValue value(String value) {
    this.value = value;
    return this;
  }

  /**
   * the value of the setting. The maximum length is 4096 characters for secrets and 8192 for normal settings.
   * @return value
  */
  @ApiModelProperty(required = true, value = "the value of the setting. The maximum length is 4096 characters for secrets and 8192 for normal settings.")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public SettingValue applyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
    return this;
  }

  /**
   * Get applyTo
   * @return applyTo
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public SettingValueApplyTo getApplyTo() {
    return applyTo;
  }

  public void setApplyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
  }

  public SettingValue createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  /**
   * the user that created the tenant
   * @return createdBy
  */
  @ApiModelProperty(readOnly = true, value = "the user that created the tenant")


  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public SettingValue createdOn(OffsetDateTime createdOn) {
    this.createdOn = createdOn;
    return this;
  }

  /**
   * the date the tenant was created
   * @return createdOn
  */
  @ApiModelProperty(readOnly = true, value = "the date the tenant was created")

  @Valid

  public OffsetDateTime getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(OffsetDateTime createdOn) {
    this.createdOn = createdOn;
  }

  public SettingValue modifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  /**
   * the user that last modified the tenant
   * @return modifiedBy
  */
  @ApiModelProperty(readOnly = true, value = "the user that last modified the tenant")


  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public SettingValue modifiedOn(OffsetDateTime modifiedOn) {
    this.modifiedOn = modifiedOn;
    return this;
  }

  /**
   * the date the tenant was last modfied
   * @return modifiedOn
  */
  @ApiModelProperty(readOnly = true, value = "the date the tenant was last modfied")

  @Valid

  public OffsetDateTime getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(OffsetDateTime modifiedOn) {
    this.modifiedOn = modifiedOn;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingValue settingValue = (SettingValue) o;
    return Objects.equals(this.id, settingValue.id) &&
        Objects.equals(this.version, settingValue.version) &&
        Objects.equals(this.applicationId, settingValue.applicationId) &&
        Objects.equals(this.name, settingValue.name) &&
        Objects.equals(this.value, settingValue.value) &&
        Objects.equals(this.applyTo, settingValue.applyTo) &&
        Objects.equals(this.createdBy, settingValue.createdBy) &&
        Objects.equals(this.createdOn, settingValue.createdOn) &&
        Objects.equals(this.modifiedBy, settingValue.modifiedBy) &&
        Objects.equals(this.modifiedOn, settingValue.modifiedOn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, applicationId, name, value, applyTo, createdBy, createdOn, modifiedBy, modifiedOn);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingValue {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    applyTo: ").append(toIndentedString(applyTo)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    createdOn: ").append(toIndentedString(createdOn)).append("\n");
    sb.append("    modifiedBy: ").append(toIndentedString(modifiedBy)).append("\n");
    sb.append("    modifiedOn: ").append(toIndentedString(modifiedOn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

