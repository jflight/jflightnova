package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.guidewire.jflightnova.client.helios.model.ApplyToLayerDeploymentAllOf;
import com.guidewire.jflightnova.client.helios.model.SettingValueApplyTo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApplyToLayerDeployment
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class ApplyToLayerDeployment extends SettingValueApplyTo  {
  @JsonProperty("deployment")
  private String deployment;

  @JsonProperty("deploymentId")
  private String deploymentId;

  public ApplyToLayerDeployment deployment(String deployment) {
    this.deployment = deployment;
    return this;
  }

  /**
   * A specific deployment within the environment level. Examples: devA, dev2, etc.
   * @return deployment
  */
  @ApiModelProperty(value = "A specific deployment within the environment level. Examples: devA, dev2, etc.")


  public String getDeployment() {
    return deployment;
  }

  public void setDeployment(String deployment) {
    this.deployment = deployment;
  }

  public ApplyToLayerDeployment deploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
    return this;
  }

  /**
   * a valid grn that specifies a deployment.  please see the helios docs for details
   * @return deploymentId
  */
  @ApiModelProperty(value = "a valid grn that specifies a deployment.  please see the helios docs for details")


  public String getDeploymentId() {
    return deploymentId;
  }

  public void setDeploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplyToLayerDeployment applyToLayerDeployment = (ApplyToLayerDeployment) o;
    return Objects.equals(this.deployment, applyToLayerDeployment.deployment) &&
        Objects.equals(this.deploymentId, applyToLayerDeployment.deploymentId) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deployment, deploymentId, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplyToLayerDeployment {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    deployment: ").append(toIndentedString(deployment)).append("\n");
    sb.append("    deploymentId: ").append(toIndentedString(deploymentId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

