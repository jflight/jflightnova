package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AuthorizeClientRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class AuthorizeClientRequest   {
  @JsonProperty("clientId")
  private String clientId;

  /**
   * Name of the GW Hub instance where the client is registered.
   */
  public enum GwHubInstanceEnum {
    STAGING("staging"),
    
    NORTH_AMERICA("north-america");

    private String value;

    GwHubInstanceEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GwHubInstanceEnum fromValue(String value) {
      for (GwHubInstanceEnum b : GwHubInstanceEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("gwHubInstance")
  private GwHubInstanceEnum gwHubInstance;

  public AuthorizeClientRequest clientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

  /**
   * client id from keti registration . Please refer docs at [Keti Docs](https://keti.internal.guidewire.net/docs/dev/register_application/)
   * @return clientId
  */
  @ApiModelProperty(required = true, value = "client id from keti registration . Please refer docs at [Keti Docs](https://keti.internal.guidewire.net/docs/dev/register_application/)")
  @NotNull

@Size(min=1) 
  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public AuthorizeClientRequest gwHubInstance(GwHubInstanceEnum gwHubInstance) {
    this.gwHubInstance = gwHubInstance;
    return this;
  }

  /**
   * Name of the GW Hub instance where the client is registered.
   * @return gwHubInstance
  */
  @ApiModelProperty(required = true, value = "Name of the GW Hub instance where the client is registered.")
  @NotNull


  public GwHubInstanceEnum getGwHubInstance() {
    return gwHubInstance;
  }

  public void setGwHubInstance(GwHubInstanceEnum gwHubInstance) {
    this.gwHubInstance = gwHubInstance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthorizeClientRequest authorizeClientRequest = (AuthorizeClientRequest) o;
    return Objects.equals(this.clientId, authorizeClientRequest.clientId) &&
        Objects.equals(this.gwHubInstance, authorizeClientRequest.gwHubInstance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(clientId, gwHubInstance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthorizeClientRequest {\n");
    
    sb.append("    clientId: ").append(toIndentedString(clientId)).append("\n");
    sb.append("    gwHubInstance: ").append(toIndentedString(gwHubInstance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

