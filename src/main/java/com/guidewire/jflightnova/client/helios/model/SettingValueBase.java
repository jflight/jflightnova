package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.SettingValueApplyTo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * base fields (i.e. not audit) for a SettingValue
 */
@ApiModel(description = "base fields (i.e. not audit) for a SettingValue")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingValueBase   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("applicationId")
  private Long applicationId;

  @JsonProperty("name")
  private String name;

  @JsonProperty("value")
  private String value;

  @JsonProperty("applyTo")
  private SettingValueApplyTo applyTo;

  public SettingValueBase id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SettingValueBase version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public SettingValueBase applicationId(Long applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * The primary key of the application this entity is associated with.
   * @return applicationId
  */
  @ApiModelProperty(required = true, value = "The primary key of the application this entity is associated with.")
  @NotNull


  public Long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(Long applicationId) {
    this.applicationId = applicationId;
  }

  public SettingValueBase name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the name of the setting.
   * @return name
  */
  @ApiModelProperty(required = true, value = "the name of the setting.")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SettingValueBase value(String value) {
    this.value = value;
    return this;
  }

  /**
   * the value of the setting. The maximum length is 4096 characters for secrets and 8192 for normal settings.
   * @return value
  */
  @ApiModelProperty(required = true, value = "the value of the setting. The maximum length is 4096 characters for secrets and 8192 for normal settings.")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public SettingValueBase applyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
    return this;
  }

  /**
   * Get applyTo
   * @return applyTo
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public SettingValueApplyTo getApplyTo() {
    return applyTo;
  }

  public void setApplyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingValueBase settingValueBase = (SettingValueBase) o;
    return Objects.equals(this.id, settingValueBase.id) &&
        Objects.equals(this.version, settingValueBase.version) &&
        Objects.equals(this.applicationId, settingValueBase.applicationId) &&
        Objects.equals(this.name, settingValueBase.name) &&
        Objects.equals(this.value, settingValueBase.value) &&
        Objects.equals(this.applyTo, settingValueBase.applyTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, applicationId, name, value, applyTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingValueBase {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    applyTo: ").append(toIndentedString(applyTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

