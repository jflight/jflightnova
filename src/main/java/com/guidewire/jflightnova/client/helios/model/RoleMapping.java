package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * used to map a user identifier (email) or group identifier (group name) from the idp to roles defined in the property service configuration.
 */
@ApiModel(description = "used to map a user identifier (email) or group identifier (group name) from the idp to roles defined in the property service configuration.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class RoleMapping extends AuditInformation  {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("roleId")
  private Long roleId;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("emailAddress")
  private String emailAddress;

  @JsonProperty("groupName")
  private String groupName;

  public RoleMapping id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public RoleMapping roleId(Long roleId) {
    this.roleId = roleId;
    return this;
  }

  /**
   * resource id of role
   * @return roleId
  */
  @ApiModelProperty(value = "resource id of role")


  public Long getRoleId() {
    return roleId;
  }

  public void setRoleId(Long roleId) {
    this.roleId = roleId;
  }

  public RoleMapping version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public RoleMapping emailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

  /**
   * the emailAddress of the user assigned to the given role. 
   * @return emailAddress
  */
  @ApiModelProperty(value = "the emailAddress of the user assigned to the given role. ")


  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public RoleMapping groupName(String groupName) {
    this.groupName = groupName;
    return this;
  }

  /**
   * the name of the group assigned to the given role. 
   * @return groupName
  */
  @ApiModelProperty(value = "the name of the group assigned to the given role. ")


  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoleMapping roleMapping = (RoleMapping) o;
    return Objects.equals(this.id, roleMapping.id) &&
        Objects.equals(this.roleId, roleMapping.roleId) &&
        Objects.equals(this.version, roleMapping.version) &&
        Objects.equals(this.emailAddress, roleMapping.emailAddress) &&
        Objects.equals(this.groupName, roleMapping.groupName) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, roleId, version, emailAddress, groupName, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoleMapping {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    roleId: ").append(toIndentedString(roleId)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("    groupName: ").append(toIndentedString(groupName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

