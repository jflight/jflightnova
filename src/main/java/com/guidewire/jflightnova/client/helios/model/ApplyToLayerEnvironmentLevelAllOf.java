package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApplyToLayerEnvironmentLevelAllOf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class ApplyToLayerEnvironmentLevelAllOf   {
  @JsonProperty("environmentLevel")
  private String environmentLevel;

  public ApplyToLayerEnvironmentLevelAllOf environmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
    return this;
  }

  /**
   * Environment level to apply settings. Examples: dev, staging, uat, prod, etc.
   * @return environmentLevel
  */
  @ApiModelProperty(required = true, value = "Environment level to apply settings. Examples: dev, staging, uat, prod, etc.")
  @NotNull


  public String getEnvironmentLevel() {
    return environmentLevel;
  }

  public void setEnvironmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplyToLayerEnvironmentLevelAllOf applyToLayerEnvironmentLevelAllOf = (ApplyToLayerEnvironmentLevelAllOf) o;
    return Objects.equals(this.environmentLevel, applyToLayerEnvironmentLevelAllOf.environmentLevel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(environmentLevel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplyToLayerEnvironmentLevelAllOf {\n");
    
    sb.append("    environmentLevel: ").append(toIndentedString(environmentLevel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

