package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.BatchSettingValueRequestOld;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A request payload that allows multiple setting values to be changes within an atomic transaction
 */
@ApiModel(description = "A request payload that allows multiple setting values to be changes within an atomic transaction")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValuesRequestOld   {
  @JsonProperty("applicationId")
  private Long applicationId;

  @JsonProperty("profile")
  private String profile = "default";

  @JsonProperty("settings")
  @Valid
  private List<BatchSettingValueRequestOld> settings = null;

  public BatchSettingValuesRequestOld applicationId(Long applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * The primary key of the application this entity is associated with.
   * @return applicationId
  */
  @ApiModelProperty(required = true, value = "The primary key of the application this entity is associated with.")
  @NotNull


  public Long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(Long applicationId) {
    this.applicationId = applicationId;
  }

  public BatchSettingValuesRequestOld profile(String profile) {
    this.profile = profile;
    return this;
  }

  /**
   * also known as layer, this is the value used to group settings of a given application deployment together.
   * @return profile
  */
  @ApiModelProperty(required = true, value = "also known as layer, this is the value used to group settings of a given application deployment together.")
  @NotNull


  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public BatchSettingValuesRequestOld settings(List<BatchSettingValueRequestOld> settings) {
    this.settings = settings;
    return this;
  }

  public BatchSettingValuesRequestOld addSettingsItem(BatchSettingValueRequestOld settingsItem) {
    if (this.settings == null) {
      this.settings = new ArrayList<>();
    }
    this.settings.add(settingsItem);
    return this;
  }

  /**
   * Get settings
   * @return settings
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<BatchSettingValueRequestOld> getSettings() {
    return settings;
  }

  public void setSettings(List<BatchSettingValueRequestOld> settings) {
    this.settings = settings;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValuesRequestOld batchSettingValuesRequestOld = (BatchSettingValuesRequestOld) o;
    return Objects.equals(this.applicationId, batchSettingValuesRequestOld.applicationId) &&
        Objects.equals(this.profile, batchSettingValuesRequestOld.profile) &&
        Objects.equals(this.settings, batchSettingValuesRequestOld.settings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(applicationId, profile, settings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValuesRequestOld {\n");
    
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("    profile: ").append(toIndentedString(profile)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

