package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * An application should be thought of as a codebase
 */
@ApiModel(description = "An application should be thought of as a codebase")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class Application extends AuditInformation  {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("name")
  private String name;

  @JsonProperty("owner")
  private String owner;

  @JsonProperty("ownerEmail")
  private String ownerEmail;

  public Application id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Application version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Application name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the name used to identify the application. if you use spring, should match the property value \"spring.application.name\"
   * @return name
  */
  @ApiModelProperty(required = true, value = "the name used to identify the application. if you use spring, should match the property value \"spring.application.name\"")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Application owner(String owner) {
    this.owner = owner;
    return this;
  }

  /**
   * (not-finalized) This should be the pod that manages the application
   * @return owner
  */
  @ApiModelProperty(required = true, value = "(not-finalized) This should be the pod that manages the application")
  @NotNull


  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Application ownerEmail(String ownerEmail) {
    this.ownerEmail = ownerEmail;
    return this;
  }

  /**
   * (not-finalized) This is an email address that will notify the the pod that manages the application
   * @return ownerEmail
  */
  @ApiModelProperty(required = true, value = "(not-finalized) This is an email address that will notify the the pod that manages the application")
  @NotNull

@Size(max=255) @javax.validation.constraints.Email
  public String getOwnerEmail() {
    return ownerEmail;
  }

  public void setOwnerEmail(String ownerEmail) {
    this.ownerEmail = ownerEmail;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Application application = (Application) o;
    return Objects.equals(this.id, application.id) &&
        Objects.equals(this.version, application.version) &&
        Objects.equals(this.name, application.name) &&
        Objects.equals(this.owner, application.owner) &&
        Objects.equals(this.ownerEmail, application.ownerEmail) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, name, owner, ownerEmail, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Application {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    ownerEmail: ").append(toIndentedString(ownerEmail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

