package com.guidewire.jflightnova.client.helios.api;

import org.springframework.cloud.openfeign.FeignClient;
import com.guidewire.jflightnova.client.helios.ClientConfiguration;

@FeignClient(name="${tenants.name:tenants}", url="${tenants.url:https://helios.api.us-east-2.service.guidewire.net/api/v1}", configuration = ClientConfiguration.class)
public interface TenantsApiClient extends TenantsApi {
}
