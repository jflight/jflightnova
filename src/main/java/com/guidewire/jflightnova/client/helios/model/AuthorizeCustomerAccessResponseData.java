package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.RoleMappingView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AuthorizeCustomerAccessResponseData
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class AuthorizeCustomerAccessResponseData   {
  @JsonProperty("mappedRoles")
  @Valid
  private List<RoleMappingView> mappedRoles = null;

  public AuthorizeCustomerAccessResponseData mappedRoles(List<RoleMappingView> mappedRoles) {
    this.mappedRoles = mappedRoles;
    return this;
  }

  public AuthorizeCustomerAccessResponseData addMappedRolesItem(RoleMappingView mappedRolesItem) {
    if (this.mappedRoles == null) {
      this.mappedRoles = new ArrayList<>();
    }
    this.mappedRoles.add(mappedRolesItem);
    return this;
  }

  /**
   * Get mappedRoles
   * @return mappedRoles
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<RoleMappingView> getMappedRoles() {
    return mappedRoles;
  }

  public void setMappedRoles(List<RoleMappingView> mappedRoles) {
    this.mappedRoles = mappedRoles;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthorizeCustomerAccessResponseData authorizeCustomerAccessResponseData = (AuthorizeCustomerAccessResponseData) o;
    return Objects.equals(this.mappedRoles, authorizeCustomerAccessResponseData.mappedRoles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mappedRoles);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthorizeCustomerAccessResponseData {\n");
    
    sb.append("    mappedRoles: ").append(toIndentedString(mappedRoles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

