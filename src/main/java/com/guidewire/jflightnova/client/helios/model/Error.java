package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.ErrorViolation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Error
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class Error   {
  @JsonProperty("entityName")
  private String entityName;

  @JsonProperty("errorKey")
  private String errorKey;

  @JsonProperty("type")
  private String type;

  @JsonProperty("title")
  private String title;

  @JsonProperty("status")
  private String status;

  @JsonProperty("message")
  private String message;

  @JsonProperty("params")
  private String params;

  @JsonProperty("violations")
  @Valid
  private List<ErrorViolation> violations = null;

  public Error entityName(String entityName) {
    this.entityName = entityName;
    return this;
  }

  /**
   * name of the setting or application
   * @return entityName
  */
  @ApiModelProperty(required = true, value = "name of the setting or application")
  @NotNull


  public String getEntityName() {
    return entityName;
  }

  public void setEntityName(String entityName) {
    this.entityName = entityName;
  }

  public Error errorKey(String errorKey) {
    this.errorKey = errorKey;
    return this;
  }

  /**
   * error key
   * @return errorKey
  */
  @ApiModelProperty(required = true, value = "error key")
  @NotNull


  public String getErrorKey() {
    return errorKey;
  }

  public void setErrorKey(String errorKey) {
    this.errorKey = errorKey;
  }

  public Error type(String type) {
    this.type = type;
    return this;
  }

  /**
   * type of error
   * @return type
  */
  @ApiModelProperty(value = "type of error")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Error title(String title) {
    this.title = title;
    return this;
  }

  /**
   * title of error
   * @return title
  */
  @ApiModelProperty(value = "title of error")


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Error status(String status) {
    this.status = status;
    return this;
  }

  /**
   * status
   * @return status
  */
  @ApiModelProperty(required = true, value = "status")
  @NotNull


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Error message(String message) {
    this.message = message;
    return this;
  }

  /**
   * error message
   * @return message
  */
  @ApiModelProperty(value = "error message")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Error params(String params) {
    this.params = params;
    return this;
  }

  /**
   * parameter
   * @return params
  */
  @ApiModelProperty(value = "parameter")


  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public Error violations(List<ErrorViolation> violations) {
    this.violations = violations;
    return this;
  }

  public Error addViolationsItem(ErrorViolation violationsItem) {
    if (this.violations == null) {
      this.violations = new ArrayList<>();
    }
    this.violations.add(violationsItem);
    return this;
  }

  /**
   * Get violations
   * @return violations
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<ErrorViolation> getViolations() {
    return violations;
  }

  public void setViolations(List<ErrorViolation> violations) {
    this.violations = violations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error error = (Error) o;
    return Objects.equals(this.entityName, error.entityName) &&
        Objects.equals(this.errorKey, error.errorKey) &&
        Objects.equals(this.type, error.type) &&
        Objects.equals(this.title, error.title) &&
        Objects.equals(this.status, error.status) &&
        Objects.equals(this.message, error.message) &&
        Objects.equals(this.params, error.params) &&
        Objects.equals(this.violations, error.violations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(entityName, errorKey, type, title, status, message, params, violations);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    entityName: ").append(toIndentedString(entityName)).append("\n");
    sb.append("    errorKey: ").append(toIndentedString(errorKey)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    params: ").append(toIndentedString(params)).append("\n");
    sb.append("    violations: ").append(toIndentedString(violations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

