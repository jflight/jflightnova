package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * used to map a user to a role.EmailAddress and groupName are mutually exclusive.
 */
@ApiModel(description = "used to map a user to a role.EmailAddress and groupName are mutually exclusive.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class RoleMappingCreateRequest   {
  @JsonProperty("emailAddress")
  private String emailAddress;

  @JsonProperty("groupName")
  private String groupName;

  @JsonProperty("role")
  private String role;

  public RoleMappingCreateRequest emailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

  /**
   * the emailAddress of the user assigned to the given role. 
   * @return emailAddress
  */
  @ApiModelProperty(value = "the emailAddress of the user assigned to the given role. ")


  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public RoleMappingCreateRequest groupName(String groupName) {
    this.groupName = groupName;
    return this;
  }

  /**
   * the group assigned to the given role. 
   * @return groupName
  */
  @ApiModelProperty(value = "the group assigned to the given role. ")


  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public RoleMappingCreateRequest role(String role) {
    this.role = role;
    return this;
  }

  /**
   * the role to give the user. currently supports the values read, write and owner.
   * @return role
  */
  @ApiModelProperty(required = true, value = "the role to give the user. currently supports the values read, write and owner.")
  @NotNull


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoleMappingCreateRequest roleMappingCreateRequest = (RoleMappingCreateRequest) o;
    return Objects.equals(this.emailAddress, roleMappingCreateRequest.emailAddress) &&
        Objects.equals(this.groupName, roleMappingCreateRequest.groupName) &&
        Objects.equals(this.role, roleMappingCreateRequest.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emailAddress, groupName, role);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoleMappingCreateRequest {\n");
    
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("    groupName: ").append(toIndentedString(groupName)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

