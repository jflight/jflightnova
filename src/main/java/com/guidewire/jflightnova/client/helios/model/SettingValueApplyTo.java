package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Specifies to which layer setting is applied . Please refer docs at [Helios Docs](https://propertyservicedocs.internal.guidewire.net/docs/dev/managing_settings/#associating-a-layer-to-a-setting-value)
 */
@ApiModel(description = "Specifies to which layer setting is applied . Please refer docs at [Helios Docs](https://propertyservicedocs.internal.guidewire.net/docs/dev/managing_settings/#associating-a-layer-to-a-setting-value)")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "layerType", visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value = ApplyToLayerTenant.class, name = "tenant_default"),
  @JsonSubTypes.Type(value = ApplyToLayerProjectEnvironment.class, name = "project"),
  @JsonSubTypes.Type(value = ApplyToLayerEnvironmentLevel.class, name = "environment_level"),
  @JsonSubTypes.Type(value = ApplyToLayerDeployment.class, name = "deployment"),
})

public class SettingValueApplyTo   {
  /**
   * Gets or Sets layerType
   */
  public enum LayerTypeEnum {
    DEPLOYMENT("deployment"),
    
    ENVIRONMENT_LEVEL("environment_level"),
    
    TENANT_DEFAULT("tenant_default");

    private String value;

    LayerTypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LayerTypeEnum fromValue(String value) {
      for (LayerTypeEnum b : LayerTypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("layerType")
  private LayerTypeEnum layerType;

  public SettingValueApplyTo layerType(LayerTypeEnum layerType) {
    this.layerType = layerType;
    return this;
  }

  /**
   * Get layerType
   * @return layerType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public LayerTypeEnum getLayerType() {
    return layerType;
  }

  public void setLayerType(LayerTypeEnum layerType) {
    this.layerType = layerType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingValueApplyTo settingValueApplyTo = (SettingValueApplyTo) o;
    return Objects.equals(this.layerType, settingValueApplyTo.layerType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(layerType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingValueApplyTo {\n");
    
    sb.append("    layerType: ").append(toIndentedString(layerType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

