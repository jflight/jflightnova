package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * contains setting name and current setting value
 */
@ApiModel(description = "contains setting name and current setting value")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class EffectiveConfigValueItem   {
  @JsonProperty("settingName")
  private String settingName;

  @JsonProperty("settingValue")
  private String settingValue;

  public EffectiveConfigValueItem settingName(String settingName) {
    this.settingName = settingName;
    return this;
  }

  /**
   * name of the setting
   * @return settingName
  */
  @ApiModelProperty(required = true, value = "name of the setting")
  @NotNull


  public String getSettingName() {
    return settingName;
  }

  public void setSettingName(String settingName) {
    this.settingName = settingName;
  }

  public EffectiveConfigValueItem settingValue(String settingValue) {
    this.settingValue = settingValue;
    return this;
  }

  /**
   * current value of the setting
   * @return settingValue
  */
  @ApiModelProperty(required = true, value = "current value of the setting")
  @NotNull


  public String getSettingValue() {
    return settingValue;
  }

  public void setSettingValue(String settingValue) {
    this.settingValue = settingValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EffectiveConfigValueItem effectiveConfigValueItem = (EffectiveConfigValueItem) o;
    return Objects.equals(this.settingName, effectiveConfigValueItem.settingName) &&
        Objects.equals(this.settingValue, effectiveConfigValueItem.settingValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(settingName, settingValue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EffectiveConfigValueItem {\n");
    
    sb.append("    settingName: ").append(toIndentedString(settingName)).append("\n");
    sb.append("    settingValue: ").append(toIndentedString(settingValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

