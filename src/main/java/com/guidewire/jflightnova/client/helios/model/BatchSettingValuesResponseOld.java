package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.SettingValueOld;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * response that details what setting values were updated in the atomic request
 */
@ApiModel(description = "response that details what setting values were updated in the atomic request")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValuesResponseOld   {
  @JsonProperty("created")
  @Valid
  private List<SettingValueOld> created = null;

  @JsonProperty("updated")
  @Valid
  private List<SettingValueOld> updated = null;

  @JsonProperty("deleted")
  @Valid
  private List<SettingValueOld> deleted = null;

  public BatchSettingValuesResponseOld created(List<SettingValueOld> created) {
    this.created = created;
    return this;
  }

  public BatchSettingValuesResponseOld addCreatedItem(SettingValueOld createdItem) {
    if (this.created == null) {
      this.created = new ArrayList<>();
    }
    this.created.add(createdItem);
    return this;
  }

  /**
   * Get created
   * @return created
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValueOld> getCreated() {
    return created;
  }

  public void setCreated(List<SettingValueOld> created) {
    this.created = created;
  }

  public BatchSettingValuesResponseOld updated(List<SettingValueOld> updated) {
    this.updated = updated;
    return this;
  }

  public BatchSettingValuesResponseOld addUpdatedItem(SettingValueOld updatedItem) {
    if (this.updated == null) {
      this.updated = new ArrayList<>();
    }
    this.updated.add(updatedItem);
    return this;
  }

  /**
   * Get updated
   * @return updated
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValueOld> getUpdated() {
    return updated;
  }

  public void setUpdated(List<SettingValueOld> updated) {
    this.updated = updated;
  }

  public BatchSettingValuesResponseOld deleted(List<SettingValueOld> deleted) {
    this.deleted = deleted;
    return this;
  }

  public BatchSettingValuesResponseOld addDeletedItem(SettingValueOld deletedItem) {
    if (this.deleted == null) {
      this.deleted = new ArrayList<>();
    }
    this.deleted.add(deletedItem);
    return this;
  }

  /**
   * Get deleted
   * @return deleted
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValueOld> getDeleted() {
    return deleted;
  }

  public void setDeleted(List<SettingValueOld> deleted) {
    this.deleted = deleted;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValuesResponseOld batchSettingValuesResponseOld = (BatchSettingValuesResponseOld) o;
    return Objects.equals(this.created, batchSettingValuesResponseOld.created) &&
        Objects.equals(this.updated, batchSettingValuesResponseOld.updated) &&
        Objects.equals(this.deleted, batchSettingValuesResponseOld.deleted);
  }

  @Override
  public int hashCode() {
    return Objects.hash(created, updated, deleted);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValuesResponseOld {\n");
    
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    updated: ").append(toIndentedString(updated)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

