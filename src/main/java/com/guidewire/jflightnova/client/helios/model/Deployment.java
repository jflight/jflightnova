package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Contains information about a deployment.
 */
@ApiModel(description = "Contains information about a deployment.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class Deployment extends AuditInformation  {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("name")
  private String name;

  @JsonProperty("environmentLevel")
  private String environmentLevel;

  @JsonProperty("supercluster")
  private String supercluster;

  @JsonProperty("galaxy")
  private String galaxy;

  @JsonProperty("quadrant")
  private String quadrant;

  @JsonProperty("planet")
  private String planet;

  @JsonProperty("planetClass")
  private String planetClass;

  @JsonProperty("project")
  private String project;

  @JsonProperty("deploymentId")
  private String deploymentId;

  @JsonProperty("description")
  private String description;

  @JsonProperty("lastClientRequest")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime lastClientRequest;

  @JsonProperty("useGrnCriteriaFormat")
  private Boolean useGrnCriteriaFormat = false;

  @JsonProperty("allowDeprecatedLookup")
  private Boolean allowDeprecatedLookup = true;

  public Deployment id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Deployment version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Deployment name(String name) {
    this.name = name;
    return this;
  }

  /**
   * The name of the deployment.
   * @return name
  */
  @ApiModelProperty(value = "The name of the deployment.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Deployment environmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
    return this;
  }

  /**
   * The environment level of the deployment - this has the same allowable values as planet class
   * @return environmentLevel
  */
  @ApiModelProperty(value = "The environment level of the deployment - this has the same allowable values as planet class")


  public String getEnvironmentLevel() {
    return environmentLevel;
  }

  public void setEnvironmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
  }

  public Deployment supercluster(String supercluster) {
    this.supercluster = supercluster;
    return this;
  }

  /**
   * Geographical region - north-america, australia, eu, etc
   * @return supercluster
  */
  @ApiModelProperty(readOnly = true, value = "Geographical region - north-america, australia, eu, etc")


  public String getSupercluster() {
    return supercluster;
  }

  public void setSupercluster(String supercluster) {
    this.supercluster = supercluster;
  }

  public Deployment galaxy(String galaxy) {
    this.galaxy = galaxy;
    return this;
  }

  /**
   * GWCP name for an AWS region (andromedia, butterfly, needle, etc)
   * @return galaxy
  */
  @ApiModelProperty(readOnly = true, value = "GWCP name for an AWS region (andromedia, butterfly, needle, etc)")


  public String getGalaxy() {
    return galaxy;
  }

  public void setGalaxy(String galaxy) {
    this.galaxy = galaxy;
  }

  public Deployment quadrant(String quadrant) {
    this.quadrant = quadrant;
    return this;
  }

  /**
   * Cluster name (alpha1, omega2, etc)
   * @return quadrant
  */
  @ApiModelProperty(readOnly = true, value = "Cluster name (alpha1, omega2, etc)")


  public String getQuadrant() {
    return quadrant;
  }

  public void setQuadrant(String quadrant) {
    this.quadrant = quadrant;
  }

  public Deployment planet(String planet) {
    this.planet = planet;
    return this;
  }

  /**
   * Planet name
   * @return planet
  */
  @ApiModelProperty(readOnly = true, value = "Planet name")


  public String getPlanet() {
    return planet;
  }

  public void setPlanet(String planet) {
    this.planet = planet;
  }

  public Deployment planetClass(String planetClass) {
    this.planetClass = planetClass;
    return this;
  }

  /**
   * Planet class - dev, staging, int, prod, etc
   * @return planetClass
  */
  @ApiModelProperty(readOnly = true, value = "Planet class - dev, staging, int, prod, etc")


  public String getPlanetClass() {
    return planetClass;
  }

  public void setPlanetClass(String planetClass) {
    this.planetClass = planetClass;
  }

  public Deployment project(String project) {
    this.project = project;
    return this;
  }

  /**
   * Insurer project
   * @return project
  */
  @ApiModelProperty(readOnly = true, value = "Insurer project")


  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public Deployment deploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
    return this;
  }

  /**
   * Identifies a deployment using GRN. See here for more information on [GRN](https://confluence.guidewire.com/pages/viewpage.action?pageId=530019775).
   * @return deploymentId
  */
  @ApiModelProperty(value = "Identifies a deployment using GRN. See here for more information on [GRN](https://confluence.guidewire.com/pages/viewpage.action?pageId=530019775).")

@Pattern(regexp="^grn\\:v1\\:.*") 
  public String getDeploymentId() {
    return deploymentId;
  }

  public void setDeploymentId(String deploymentId) {
    this.deploymentId = deploymentId;
  }

  public Deployment description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of use or purpose.
   * @return description
  */
  @ApiModelProperty(value = "Description of use or purpose.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Deployment lastClientRequest(OffsetDateTime lastClientRequest) {
    this.lastClientRequest = lastClientRequest;
    return this;
  }

  /**
   * Date/time that configuration for this deployment was last requested. This is a temporary field which will be removed without notice!
   * @return lastClientRequest
  */
  @ApiModelProperty(readOnly = true, value = "Date/time that configuration for this deployment was last requested. This is a temporary field which will be removed without notice!")

  @Valid

  public OffsetDateTime getLastClientRequest() {
    return lastClientRequest;
  }

  public void setLastClientRequest(OffsetDateTime lastClientRequest) {
    this.lastClientRequest = lastClientRequest;
  }

  public Deployment useGrnCriteriaFormat(Boolean useGrnCriteriaFormat) {
    this.useGrnCriteriaFormat = useGrnCriteriaFormat;
    return this;
  }

  /**
   * Used to track status of internal status GRN migration. This field is only temporary and will be removed in a future release without notice. Please ignore it. Only users with admin rights can update this flag.
   * @return useGrnCriteriaFormat
  */
  @ApiModelProperty(readOnly = true, value = "Used to track status of internal status GRN migration. This field is only temporary and will be removed in a future release without notice. Please ignore it. Only users with admin rights can update this flag.")


  public Boolean getUseGrnCriteriaFormat() {
    return useGrnCriteriaFormat;
  }

  public void setUseGrnCriteriaFormat(Boolean useGrnCriteriaFormat) {
    this.useGrnCriteriaFormat = useGrnCriteriaFormat;
  }

  public Deployment allowDeprecatedLookup(Boolean allowDeprecatedLookup) {
    this.allowDeprecatedLookup = allowDeprecatedLookup;
    return this;
  }

  /**
   * Used to track status of internal status GRN migration. This field is only temporary and will be removed in a future release without notice. Please ignore it. Only users with admin rights can update this flag.
   * @return allowDeprecatedLookup
  */
  @ApiModelProperty(readOnly = true, value = "Used to track status of internal status GRN migration. This field is only temporary and will be removed in a future release without notice. Please ignore it. Only users with admin rights can update this flag.")


  public Boolean getAllowDeprecatedLookup() {
    return allowDeprecatedLookup;
  }

  public void setAllowDeprecatedLookup(Boolean allowDeprecatedLookup) {
    this.allowDeprecatedLookup = allowDeprecatedLookup;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Deployment deployment = (Deployment) o;
    return Objects.equals(this.id, deployment.id) &&
        Objects.equals(this.version, deployment.version) &&
        Objects.equals(this.name, deployment.name) &&
        Objects.equals(this.environmentLevel, deployment.environmentLevel) &&
        Objects.equals(this.supercluster, deployment.supercluster) &&
        Objects.equals(this.galaxy, deployment.galaxy) &&
        Objects.equals(this.quadrant, deployment.quadrant) &&
        Objects.equals(this.planet, deployment.planet) &&
        Objects.equals(this.planetClass, deployment.planetClass) &&
        Objects.equals(this.project, deployment.project) &&
        Objects.equals(this.deploymentId, deployment.deploymentId) &&
        Objects.equals(this.description, deployment.description) &&
        Objects.equals(this.lastClientRequest, deployment.lastClientRequest) &&
        Objects.equals(this.useGrnCriteriaFormat, deployment.useGrnCriteriaFormat) &&
        Objects.equals(this.allowDeprecatedLookup, deployment.allowDeprecatedLookup) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, name, environmentLevel, supercluster, galaxy, quadrant, planet, planetClass, project, deploymentId, description, lastClientRequest, useGrnCriteriaFormat, allowDeprecatedLookup, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Deployment {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    environmentLevel: ").append(toIndentedString(environmentLevel)).append("\n");
    sb.append("    supercluster: ").append(toIndentedString(supercluster)).append("\n");
    sb.append("    galaxy: ").append(toIndentedString(galaxy)).append("\n");
    sb.append("    quadrant: ").append(toIndentedString(quadrant)).append("\n");
    sb.append("    planet: ").append(toIndentedString(planet)).append("\n");
    sb.append("    planetClass: ").append(toIndentedString(planetClass)).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    deploymentId: ").append(toIndentedString(deploymentId)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    lastClientRequest: ").append(toIndentedString(lastClientRequest)).append("\n");
    sb.append("    useGrnCriteriaFormat: ").append(toIndentedString(useGrnCriteriaFormat)).append("\n");
    sb.append("    allowDeprecatedLookup: ").append(toIndentedString(allowDeprecatedLookup)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

