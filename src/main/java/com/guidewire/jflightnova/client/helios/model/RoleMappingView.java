package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Maps a role to a user or group (mutually exclusive).
 */
@ApiModel(description = "Maps a role to a user or group (mutually exclusive).")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class RoleMappingView extends AuditInformation  {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("emailAddress")
  private String emailAddress;

  @JsonProperty("groupName")
  private String groupName;

  @JsonProperty("role")
  private String role;

  public RoleMappingView id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public RoleMappingView emailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

  /**
   * The email address of the user
   * @return emailAddress
  */
  @ApiModelProperty(value = "The email address of the user")


  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public RoleMappingView groupName(String groupName) {
    this.groupName = groupName;
    return this;
  }

  /**
   * The name of the group
   * @return groupName
  */
  @ApiModelProperty(value = "The name of the group")


  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public RoleMappingView role(String role) {
    this.role = role;
    return this;
  }

  /**
   * The name of the role granted to the user or group
   * @return role
  */
  @ApiModelProperty(value = "The name of the role granted to the user or group")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoleMappingView roleMappingView = (RoleMappingView) o;
    return Objects.equals(this.id, roleMappingView.id) &&
        Objects.equals(this.emailAddress, roleMappingView.emailAddress) &&
        Objects.equals(this.groupName, roleMappingView.groupName) &&
        Objects.equals(this.role, roleMappingView.role) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, emailAddress, groupName, role, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoleMappingView {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("    groupName: ").append(toIndentedString(groupName)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

