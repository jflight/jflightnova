package com.guidewire.jflightnova.client.helios;

import com.guidewire.jflightnova.client.ExcludeFromComponentScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

@Configuration
@ExcludeFromComponentScan
@EnableConfigurationProperties
public class ClientConfiguration {

    @Value("${spring.security.oauth2.issuer-uri}")
    private String issuerUri;

    @Value("${spring.security.oauth2.client-id}")
    private String clientId;

    @Value("${spring.security.oauth2.client-secret}")
    private String clientSecret;

    @Bean
    public OAuth2FeignRequestInterceptor guidewireHubRequestInterceptor() {
        return new OAuth2FeignRequestInterceptor(new DefaultOAuth2ClientContext(), guidewireHubResourceDetails());
    }

    @Bean
    @ConfigurationProperties("helios")
    public ClientCredentialsResourceDetails guidewireHubResourceDetails() {
        ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
        details.setAccessTokenUri(issuerUri + "/v1/token");
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        return details;
    }
}
