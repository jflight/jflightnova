package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.Operation;
import com.guidewire.jflightnova.client.helios.model.SettingValueBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * An audit record for a setting value corresponding to the cross layer query.
 */
@ApiModel(description = "An audit record for a setting value corresponding to the cross layer query.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingDefinitionHistoryQueryResponseItem   {
  @JsonProperty("operation")
  private Operation operation;

  @JsonProperty("who")
  private String who;

  @JsonProperty("when")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime when;

  @JsonProperty("settingValue")
  private SettingValueBase settingValue;

  public SettingDefinitionHistoryQueryResponseItem operation(Operation operation) {
    this.operation = operation;
    return this;
  }

  /**
   * Get operation
   * @return operation
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public SettingDefinitionHistoryQueryResponseItem who(String who) {
    this.who = who;
    return this;
  }

  /**
   * Get who
   * @return who
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getWho() {
    return who;
  }

  public void setWho(String who) {
    this.who = who;
  }

  public SettingDefinitionHistoryQueryResponseItem when(OffsetDateTime when) {
    this.when = when;
    return this;
  }

  /**
   * Get when
   * @return when
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getWhen() {
    return when;
  }

  public void setWhen(OffsetDateTime when) {
    this.when = when;
  }

  public SettingDefinitionHistoryQueryResponseItem settingValue(SettingValueBase settingValue) {
    this.settingValue = settingValue;
    return this;
  }

  /**
   * Get settingValue
   * @return settingValue
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public SettingValueBase getSettingValue() {
    return settingValue;
  }

  public void setSettingValue(SettingValueBase settingValue) {
    this.settingValue = settingValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingDefinitionHistoryQueryResponseItem settingDefinitionHistoryQueryResponseItem = (SettingDefinitionHistoryQueryResponseItem) o;
    return Objects.equals(this.operation, settingDefinitionHistoryQueryResponseItem.operation) &&
        Objects.equals(this.who, settingDefinitionHistoryQueryResponseItem.who) &&
        Objects.equals(this.when, settingDefinitionHistoryQueryResponseItem.when) &&
        Objects.equals(this.settingValue, settingDefinitionHistoryQueryResponseItem.settingValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operation, who, when, settingValue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingDefinitionHistoryQueryResponseItem {\n");
    
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("    who: ").append(toIndentedString(who)).append("\n");
    sb.append("    when: ").append(toIndentedString(when)).append("\n");
    sb.append("    settingValue: ").append(toIndentedString(settingValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

