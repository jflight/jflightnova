package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.guidewire.jflightnova.client.helios.model.SettingValueApplyTo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * the operation to perform on a setting value within the batch request.
 */
@ApiModel(description = "the operation to perform on a setting value within the batch request.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValueRequest   {
  /**
   * Gets or Sets operation
   */
  public enum OperationEnum {
    CREATE("create"),
    
    UPDATE("update"),
    
    DELETE("delete");

    private String value;

    OperationEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static OperationEnum fromValue(String value) {
      for (OperationEnum b : OperationEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("operation")
  private OperationEnum operation;

  @JsonProperty("applicationId")
  private Long applicationId;

  @JsonProperty("name")
  private String name;

  @JsonProperty("value")
  private String value;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("applyTo")
  private SettingValueApplyTo applyTo;

  public BatchSettingValueRequest operation(OperationEnum operation) {
    this.operation = operation;
    return this;
  }

  /**
   * Get operation
   * @return operation
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public OperationEnum getOperation() {
    return operation;
  }

  public void setOperation(OperationEnum operation) {
    this.operation = operation;
  }

  public BatchSettingValueRequest applicationId(Long applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * The primary key of the application this entity is associated with.
   * @return applicationId
  */
  @ApiModelProperty(required = true, value = "The primary key of the application this entity is associated with.")
  @NotNull


  public Long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(Long applicationId) {
    this.applicationId = applicationId;
  }

  public BatchSettingValueRequest name(String name) {
    this.name = name;
    return this;
  }

  /**
   * the name of the setting.
   * @return name
  */
  @ApiModelProperty(required = true, value = "the name of the setting.")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BatchSettingValueRequest value(String value) {
    this.value = value;
    return this;
  }

  /**
   * the value of the setting. This field is required for create and update operations.
   * @return value
  */
  @ApiModelProperty(value = "the value of the setting. This field is required for create and update operations.")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public BatchSettingValueRequest version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public BatchSettingValueRequest applyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
    return this;
  }

  /**
   * Get applyTo
   * @return applyTo
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public SettingValueApplyTo getApplyTo() {
    return applyTo;
  }

  public void setApplyTo(SettingValueApplyTo applyTo) {
    this.applyTo = applyTo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValueRequest batchSettingValueRequest = (BatchSettingValueRequest) o;
    return Objects.equals(this.operation, batchSettingValueRequest.operation) &&
        Objects.equals(this.applicationId, batchSettingValueRequest.applicationId) &&
        Objects.equals(this.name, batchSettingValueRequest.name) &&
        Objects.equals(this.value, batchSettingValueRequest.value) &&
        Objects.equals(this.version, batchSettingValueRequest.version) &&
        Objects.equals(this.applyTo, batchSettingValueRequest.applyTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operation, applicationId, name, value, version, applyTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValueRequest {\n");
    
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    applyTo: ").append(toIndentedString(applyTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

