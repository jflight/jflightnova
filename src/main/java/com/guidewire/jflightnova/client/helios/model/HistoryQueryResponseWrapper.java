package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.HistoryQueryResponse;
import com.guidewire.jflightnova.client.helios.model.HistoryQueryResponseWrapperAllOf;
import com.guidewire.jflightnova.client.helios.model.ListWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Wrapper for HistoryQueryResponseWrapper 
 */
@ApiModel(description = "Wrapper for HistoryQueryResponseWrapper ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class HistoryQueryResponseWrapper   {
  @JsonProperty("totalItems")
  private Long totalItems = 0l;

  @JsonProperty("totalPages")
  private Integer totalPages = 0;

  @JsonProperty("nextPageOffset")
  private Integer nextPageOffset = 0;

  @JsonProperty("previousPageOffset")
  private Integer previousPageOffset = 0;

  @JsonProperty("data")
  @Valid
  private List<HistoryQueryResponse> data = new ArrayList<>();

  public HistoryQueryResponseWrapper totalItems(Long totalItems) {
    this.totalItems = totalItems;
    return this;
  }

  /**
   * the number of items matching the specified criteria.
   * @return totalItems
  */
  @ApiModelProperty(readOnly = true, value = "the number of items matching the specified criteria.")


  public Long getTotalItems() {
    return totalItems;
  }

  public void setTotalItems(Long totalItems) {
    this.totalItems = totalItems;
  }

  public HistoryQueryResponseWrapper totalPages(Integer totalPages) {
    this.totalPages = totalPages;
    return this;
  }

  /**
   * the number of pages of data that contain items matching the specified criteria.
   * @return totalPages
  */
  @ApiModelProperty(readOnly = true, value = "the number of pages of data that contain items matching the specified criteria.")


  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }

  public HistoryQueryResponseWrapper nextPageOffset(Integer nextPageOffset) {
    this.nextPageOffset = nextPageOffset;
    return this;
  }

  /**
   * value to pass into pageOffset to get the next page of items
   * @return nextPageOffset
  */
  @ApiModelProperty(readOnly = true, value = "value to pass into pageOffset to get the next page of items")


  public Integer getNextPageOffset() {
    return nextPageOffset;
  }

  public void setNextPageOffset(Integer nextPageOffset) {
    this.nextPageOffset = nextPageOffset;
  }

  public HistoryQueryResponseWrapper previousPageOffset(Integer previousPageOffset) {
    this.previousPageOffset = previousPageOffset;
    return this;
  }

  /**
   * value to pass into pageOffset to get the previous page of items
   * @return previousPageOffset
  */
  @ApiModelProperty(readOnly = true, value = "value to pass into pageOffset to get the previous page of items")


  public Integer getPreviousPageOffset() {
    return previousPageOffset;
  }

  public void setPreviousPageOffset(Integer previousPageOffset) {
    this.previousPageOffset = previousPageOffset;
  }

  public HistoryQueryResponseWrapper data(List<HistoryQueryResponse> data) {
    this.data = data;
    return this;
  }

  public HistoryQueryResponseWrapper addDataItem(HistoryQueryResponse dataItem) {
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<HistoryQueryResponse> getData() {
    return data;
  }

  public void setData(List<HistoryQueryResponse> data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HistoryQueryResponseWrapper historyQueryResponseWrapper = (HistoryQueryResponseWrapper) o;
    return Objects.equals(this.totalItems, historyQueryResponseWrapper.totalItems) &&
        Objects.equals(this.totalPages, historyQueryResponseWrapper.totalPages) &&
        Objects.equals(this.nextPageOffset, historyQueryResponseWrapper.nextPageOffset) &&
        Objects.equals(this.previousPageOffset, historyQueryResponseWrapper.previousPageOffset) &&
        Objects.equals(this.data, historyQueryResponseWrapper.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalItems, totalPages, nextPageOffset, previousPageOffset, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HistoryQueryResponseWrapper {\n");
    
    sb.append("    totalItems: ").append(toIndentedString(totalItems)).append("\n");
    sb.append("    totalPages: ").append(toIndentedString(totalPages)).append("\n");
    sb.append("    nextPageOffset: ").append(toIndentedString(nextPageOffset)).append("\n");
    sb.append("    previousPageOffset: ").append(toIndentedString(previousPageOffset)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

