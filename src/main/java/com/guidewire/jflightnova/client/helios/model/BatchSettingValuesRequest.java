package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.BatchSettingValueRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A request payload that allows multiple setting values to be changes within an atomic transaction
 */
@ApiModel(description = "A request payload that allows multiple setting values to be changes within an atomic transaction")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValuesRequest   {
  @JsonProperty("settingsValues")
  @Valid
  private List<BatchSettingValueRequest> settingsValues = null;

  public BatchSettingValuesRequest settingsValues(List<BatchSettingValueRequest> settingsValues) {
    this.settingsValues = settingsValues;
    return this;
  }

  public BatchSettingValuesRequest addSettingsValuesItem(BatchSettingValueRequest settingsValuesItem) {
    if (this.settingsValues == null) {
      this.settingsValues = new ArrayList<>();
    }
    this.settingsValues.add(settingsValuesItem);
    return this;
  }

  /**
   * Get settingsValues
   * @return settingsValues
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<BatchSettingValueRequest> getSettingsValues() {
    return settingsValues;
  }

  public void setSettingsValues(List<BatchSettingValueRequest> settingsValues) {
    this.settingsValues = settingsValues;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValuesRequest batchSettingValuesRequest = (BatchSettingValuesRequest) o;
    return Objects.equals(this.settingsValues, batchSettingValuesRequest.settingsValues);
  }

  @Override
  public int hashCode() {
    return Objects.hash(settingsValues);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValuesRequest {\n");
    
    sb.append("    settingsValues: ").append(toIndentedString(settingsValues)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

