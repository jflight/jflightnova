package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * contains the value for a given setting for a given application and deployment (environment or a layer of an environment)
 */
@ApiModel(description = "contains the value for a given setting for a given application and deployment (environment or a layer of an environment)")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingValueOld extends AuditInformation  {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("applicationId")
  private Long applicationId;

  @JsonProperty("profile")
  private String profile = "default";

  @JsonProperty("label")
  private String label = "master";

  @JsonProperty("key")
  private String key;

  @JsonProperty("value")
  private String value;

  public SettingValueOld id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SettingValueOld version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public SettingValueOld applicationId(Long applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * The primary key of the application this entity is associated with.
   * @return applicationId
  */
  @ApiModelProperty(value = "The primary key of the application this entity is associated with.")


  public Long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(Long applicationId) {
    this.applicationId = applicationId;
  }

  public SettingValueOld profile(String profile) {
    this.profile = profile;
    return this;
  }

  /**
   * also known as layer, this is the value used to group settings of a given application deployment together.
   * @return profile
  */
  @ApiModelProperty(value = "also known as layer, this is the value used to group settings of a given application deployment together.")


  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public SettingValueOld label(String label) {
    this.label = label;
    return this;
  }

  /**
   * not used. ignore.
   * @return label
  */
  @ApiModelProperty(readOnly = true, value = "not used. ignore.")


  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public SettingValueOld key(String key) {
    this.key = key;
    return this;
  }

  /**
   * the name of the setting.
   * @return key
  */
  @ApiModelProperty(value = "the name of the setting.")

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public SettingValueOld value(String value) {
    this.value = value;
    return this;
  }

  /**
   * the value of the setting.
   * @return value
  */
  @ApiModelProperty(value = "the value of the setting.")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingValueOld settingValueOld = (SettingValueOld) o;
    return Objects.equals(this.id, settingValueOld.id) &&
        Objects.equals(this.version, settingValueOld.version) &&
        Objects.equals(this.applicationId, settingValueOld.applicationId) &&
        Objects.equals(this.profile, settingValueOld.profile) &&
        Objects.equals(this.label, settingValueOld.label) &&
        Objects.equals(this.key, settingValueOld.key) &&
        Objects.equals(this.value, settingValueOld.value) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, applicationId, profile, label, key, value, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingValueOld {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("    profile: ").append(toIndentedString(profile)).append("\n");
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

