package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.guidewire.jflightnova.client.helios.model.ApplyToLayerProjectEnvironmentAllOf;
import com.guidewire.jflightnova.client.helios.model.SettingValueApplyTo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApplyToLayerProjectEnvironment
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class ApplyToLayerProjectEnvironment extends SettingValueApplyTo  {
  @JsonProperty("project")
  private String project;

  @JsonProperty("environmentLevel")
  private String environmentLevel;

  public ApplyToLayerProjectEnvironment project(String project) {
    this.project = project;
    return this;
  }

  /**
   *  Project to apply settings
   * @return project
  */
  @ApiModelProperty(required = true, value = " Project to apply settings")
  @NotNull


  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public ApplyToLayerProjectEnvironment environmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
    return this;
  }

  /**
   * Environment level for the setting values. Examples: dev, staging, uat, prod, etc..
   * @return environmentLevel
  */
  @ApiModelProperty(required = true, value = "Environment level for the setting values. Examples: dev, staging, uat, prod, etc..")
  @NotNull


  public String getEnvironmentLevel() {
    return environmentLevel;
  }

  public void setEnvironmentLevel(String environmentLevel) {
    this.environmentLevel = environmentLevel;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplyToLayerProjectEnvironment applyToLayerProjectEnvironment = (ApplyToLayerProjectEnvironment) o;
    return Objects.equals(this.project, applyToLayerProjectEnvironment.project) &&
        Objects.equals(this.environmentLevel, applyToLayerProjectEnvironment.environmentLevel) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(project, environmentLevel, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplyToLayerProjectEnvironment {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    environmentLevel: ").append(toIndentedString(environmentLevel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

