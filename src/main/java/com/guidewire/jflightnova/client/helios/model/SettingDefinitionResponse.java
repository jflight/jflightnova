package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.AuditInformation;
import com.guidewire.jflightnova.client.helios.model.SettingDefinition;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * describes a setting for a given application.
 */
@ApiModel(description = "describes a setting for a given application.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingDefinitionResponse   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("version")
  private Integer version;

  @JsonProperty("settingName")
  private String settingName;

  @JsonProperty("isSecret")
  private Boolean isSecret = false;

  @JsonProperty("settingDescription")
  private String settingDescription;

  @JsonProperty("isTenantSpecific")
  private Boolean isTenantSpecific = false;

  @JsonProperty("createdBy")
  private String createdBy;

  @JsonProperty("createdOn")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime createdOn;

  @JsonProperty("modifiedBy")
  private String modifiedBy;

  @JsonProperty("modifiedOn")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime modifiedOn;

  public SettingDefinitionResponse id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * primary key
   * @return id
  */
  @ApiModelProperty(readOnly = true, value = "primary key")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SettingDefinitionResponse version(Integer version) {
    this.version = version;
    return this;
  }

  /**
   * Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.
   * @return version
  */
  @ApiModelProperty(value = "Used for optimistic locking. Only needed for updates. Expected to be the same as the value returned from the read.")


  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public SettingDefinitionResponse settingName(String settingName) {
    this.settingName = settingName;
    return this;
  }

  /**
   * name of the setting this metadata is associated with
   * @return settingName
  */
  @ApiModelProperty(required = true, value = "name of the setting this metadata is associated with")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getSettingName() {
    return settingName;
  }

  public void setSettingName(String settingName) {
    this.settingName = settingName;
  }

  public SettingDefinitionResponse isSecret(Boolean isSecret) {
    this.isSecret = isSecret;
    return this;
  }

  /**
   * If true, the associated setting is considered as a secret. <p/> Update restrictions apply to this field: <ul>   <li>Only callers with admin-global permission can change this flag from true to false.</li>   <li>This flag cannot be updated when the metadata is associated with a setting.</li> </ul>
   * @return isSecret
  */
  @ApiModelProperty(value = "If true, the associated setting is considered as a secret. <p/> Update restrictions apply to this field: <ul>   <li>Only callers with admin-global permission can change this flag from true to false.</li>   <li>This flag cannot be updated when the metadata is associated with a setting.</li> </ul>")


  public Boolean getIsSecret() {
    return isSecret;
  }

  public void setIsSecret(Boolean isSecret) {
    this.isSecret = isSecret;
  }

  public SettingDefinitionResponse settingDescription(String settingDescription) {
    this.settingDescription = settingDescription;
    return this;
  }

  /**
   * A description of the setting that this metadata is associated with
   * @return settingDescription
  */
  @ApiModelProperty(required = true, value = "A description of the setting that this metadata is associated with")
  @NotNull


  public String getSettingDescription() {
    return settingDescription;
  }

  public void setSettingDescription(String settingDescription) {
    this.settingDescription = settingDescription;
  }

  public SettingDefinitionResponse isTenantSpecific(Boolean isTenantSpecific) {
    this.isTenantSpecific = isTenantSpecific;
    return this;
  }

  /**
   * Whether this setting is present only for the current tenant, ie - hidden from other tenants.
   * @return isTenantSpecific
  */
  @ApiModelProperty(value = "Whether this setting is present only for the current tenant, ie - hidden from other tenants.")


  public Boolean getIsTenantSpecific() {
    return isTenantSpecific;
  }

  public void setIsTenantSpecific(Boolean isTenantSpecific) {
    this.isTenantSpecific = isTenantSpecific;
  }

  public SettingDefinitionResponse createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  /**
   * the user that created the tenant
   * @return createdBy
  */
  @ApiModelProperty(readOnly = true, value = "the user that created the tenant")


  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public SettingDefinitionResponse createdOn(OffsetDateTime createdOn) {
    this.createdOn = createdOn;
    return this;
  }

  /**
   * the date the tenant was created
   * @return createdOn
  */
  @ApiModelProperty(readOnly = true, value = "the date the tenant was created")

  @Valid

  public OffsetDateTime getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(OffsetDateTime createdOn) {
    this.createdOn = createdOn;
  }

  public SettingDefinitionResponse modifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  /**
   * the user that last modified the tenant
   * @return modifiedBy
  */
  @ApiModelProperty(readOnly = true, value = "the user that last modified the tenant")


  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public SettingDefinitionResponse modifiedOn(OffsetDateTime modifiedOn) {
    this.modifiedOn = modifiedOn;
    return this;
  }

  /**
   * the date the tenant was last modfied
   * @return modifiedOn
  */
  @ApiModelProperty(readOnly = true, value = "the date the tenant was last modfied")

  @Valid

  public OffsetDateTime getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(OffsetDateTime modifiedOn) {
    this.modifiedOn = modifiedOn;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingDefinitionResponse settingDefinitionResponse = (SettingDefinitionResponse) o;
    return Objects.equals(this.id, settingDefinitionResponse.id) &&
        Objects.equals(this.version, settingDefinitionResponse.version) &&
        Objects.equals(this.settingName, settingDefinitionResponse.settingName) &&
        Objects.equals(this.isSecret, settingDefinitionResponse.isSecret) &&
        Objects.equals(this.settingDescription, settingDefinitionResponse.settingDescription) &&
        Objects.equals(this.isTenantSpecific, settingDefinitionResponse.isTenantSpecific) &&
        Objects.equals(this.createdBy, settingDefinitionResponse.createdBy) &&
        Objects.equals(this.createdOn, settingDefinitionResponse.createdOn) &&
        Objects.equals(this.modifiedBy, settingDefinitionResponse.modifiedBy) &&
        Objects.equals(this.modifiedOn, settingDefinitionResponse.modifiedOn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, version, settingName, isSecret, settingDescription, isTenantSpecific, createdBy, createdOn, modifiedBy, modifiedOn);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingDefinitionResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    settingName: ").append(toIndentedString(settingName)).append("\n");
    sb.append("    isSecret: ").append(toIndentedString(isSecret)).append("\n");
    sb.append("    settingDescription: ").append(toIndentedString(settingDescription)).append("\n");
    sb.append("    isTenantSpecific: ").append(toIndentedString(isTenantSpecific)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    createdOn: ").append(toIndentedString(createdOn)).append("\n");
    sb.append("    modifiedBy: ").append(toIndentedString(modifiedBy)).append("\n");
    sb.append("    modifiedOn: ").append(toIndentedString(modifiedOn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

