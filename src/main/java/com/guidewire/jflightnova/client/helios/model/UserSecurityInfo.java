package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserSecurityInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class UserSecurityInfo   {
  @JsonProperty("userId")
  private String userId;

  @JsonProperty("tenantId")
  private String tenantId;

  @JsonProperty("authorities")
  @Valid
  private List<String> authorities = null;

  public UserSecurityInfo userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * the user identifier (externally defined)
   * @return userId
  */
  @ApiModelProperty(value = "the user identifier (externally defined)")


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public UserSecurityInfo tenantId(String tenantId) {
    this.tenantId = tenantId;
    return this;
  }

  /**
   * the tenant id associated with the request
   * @return tenantId
  */
  @ApiModelProperty(value = "the tenant id associated with the request")


  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  public UserSecurityInfo authorities(List<String> authorities) {
    this.authorities = authorities;
    return this;
  }

  public UserSecurityInfo addAuthoritiesItem(String authoritiesItem) {
    if (this.authorities == null) {
      this.authorities = new ArrayList<>();
    }
    this.authorities.add(authoritiesItem);
    return this;
  }

  /**
   * the authorities given to the user
   * @return authorities
  */
  @ApiModelProperty(value = "the authorities given to the user")


  public List<String> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(List<String> authorities) {
    this.authorities = authorities;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserSecurityInfo userSecurityInfo = (UserSecurityInfo) o;
    return Objects.equals(this.userId, userSecurityInfo.userId) &&
        Objects.equals(this.tenantId, userSecurityInfo.tenantId) &&
        Objects.equals(this.authorities, userSecurityInfo.authorities);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, tenantId, authorities);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserSecurityInfo {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
    sb.append("    authorities: ").append(toIndentedString(authorities)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

