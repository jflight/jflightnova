package com.guidewire.jflightnova.client.helios.api;

import org.springframework.cloud.openfeign.FeignClient;
import com.guidewire.jflightnova.client.helios.ClientConfiguration;

@FeignClient(name="${settingValues.name:settingValues}", url="${settingValues.url:https://helios.api.us-east-2.service.guidewire.net/api/v1}", configuration = ClientConfiguration.class)
public interface SettingValuesApiClient extends SettingValuesApi {
}
