package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.guidewire.jflightnova.client.helios.model.SettingValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * response that details what setting values were updated in the atomic request
 */
@ApiModel(description = "response that details what setting values were updated in the atomic request")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValuesResponse   {
  @JsonProperty("created")
  @Valid
  private List<SettingValue> created = null;

  @JsonProperty("updated")
  @Valid
  private List<SettingValue> updated = null;

  @JsonProperty("deleted")
  @Valid
  private List<SettingValue> deleted = null;

  public BatchSettingValuesResponse created(List<SettingValue> created) {
    this.created = created;
    return this;
  }

  public BatchSettingValuesResponse addCreatedItem(SettingValue createdItem) {
    if (this.created == null) {
      this.created = new ArrayList<>();
    }
    this.created.add(createdItem);
    return this;
  }

  /**
   * Get created
   * @return created
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValue> getCreated() {
    return created;
  }

  public void setCreated(List<SettingValue> created) {
    this.created = created;
  }

  public BatchSettingValuesResponse updated(List<SettingValue> updated) {
    this.updated = updated;
    return this;
  }

  public BatchSettingValuesResponse addUpdatedItem(SettingValue updatedItem) {
    if (this.updated == null) {
      this.updated = new ArrayList<>();
    }
    this.updated.add(updatedItem);
    return this;
  }

  /**
   * Get updated
   * @return updated
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValue> getUpdated() {
    return updated;
  }

  public void setUpdated(List<SettingValue> updated) {
    this.updated = updated;
  }

  public BatchSettingValuesResponse deleted(List<SettingValue> deleted) {
    this.deleted = deleted;
    return this;
  }

  public BatchSettingValuesResponse addDeletedItem(SettingValue deletedItem) {
    if (this.deleted == null) {
      this.deleted = new ArrayList<>();
    }
    this.deleted.add(deletedItem);
    return this;
  }

  /**
   * Get deleted
   * @return deleted
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SettingValue> getDeleted() {
    return deleted;
  }

  public void setDeleted(List<SettingValue> deleted) {
    this.deleted = deleted;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValuesResponse batchSettingValuesResponse = (BatchSettingValuesResponse) o;
    return Objects.equals(this.created, batchSettingValuesResponse.created) &&
        Objects.equals(this.updated, batchSettingValuesResponse.updated) &&
        Objects.equals(this.deleted, batchSettingValuesResponse.deleted);
  }

  @Override
  public int hashCode() {
    return Objects.hash(created, updated, deleted);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValuesResponse {\n");
    
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    updated: ").append(toIndentedString(updated)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

