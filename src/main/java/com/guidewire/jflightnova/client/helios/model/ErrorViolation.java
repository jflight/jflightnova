package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ErrorViolation
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class ErrorViolation   {
  @JsonProperty("field")
  private String field;

  @JsonProperty("message")
  private String message;

  public ErrorViolation field(String field) {
    this.field = field;
    return this;
  }

  /**
   * field that has the violation
   * @return field
  */
  @ApiModelProperty(value = "field that has the violation")


  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public ErrorViolation message(String message) {
    this.message = message;
    return this;
  }

  /**
   * error message
   * @return message
  */
  @ApiModelProperty(value = "error message")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorViolation errorViolation = (ErrorViolation) o;
    return Objects.equals(this.field, errorViolation.field) &&
        Objects.equals(this.message, errorViolation.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(field, message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorViolation {\n");
    
    sb.append("    field: ").append(toIndentedString(field)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

