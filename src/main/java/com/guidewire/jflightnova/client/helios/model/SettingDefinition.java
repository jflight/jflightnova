package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SettingDefinition
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class SettingDefinition   {
  @JsonProperty("settingName")
  private String settingName;

  @JsonProperty("isSecret")
  private Boolean isSecret = false;

  @JsonProperty("settingDescription")
  private String settingDescription;

  @JsonProperty("isTenantSpecific")
  private Boolean isTenantSpecific = false;

  public SettingDefinition settingName(String settingName) {
    this.settingName = settingName;
    return this;
  }

  /**
   * name of the setting this metadata is associated with
   * @return settingName
  */
  @ApiModelProperty(required = true, value = "name of the setting this metadata is associated with")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getSettingName() {
    return settingName;
  }

  public void setSettingName(String settingName) {
    this.settingName = settingName;
  }

  public SettingDefinition isSecret(Boolean isSecret) {
    this.isSecret = isSecret;
    return this;
  }

  /**
   * If true, the associated setting is considered as a secret. <p/> Update restrictions apply to this field: <ul>   <li>Only callers with admin-global permission can change this flag from true to false.</li>   <li>This flag cannot be updated when the metadata is associated with a setting.</li> </ul>
   * @return isSecret
  */
  @ApiModelProperty(value = "If true, the associated setting is considered as a secret. <p/> Update restrictions apply to this field: <ul>   <li>Only callers with admin-global permission can change this flag from true to false.</li>   <li>This flag cannot be updated when the metadata is associated with a setting.</li> </ul>")


  public Boolean getIsSecret() {
    return isSecret;
  }

  public void setIsSecret(Boolean isSecret) {
    this.isSecret = isSecret;
  }

  public SettingDefinition settingDescription(String settingDescription) {
    this.settingDescription = settingDescription;
    return this;
  }

  /**
   * A description of the setting that this metadata is associated with
   * @return settingDescription
  */
  @ApiModelProperty(required = true, value = "A description of the setting that this metadata is associated with")
  @NotNull


  public String getSettingDescription() {
    return settingDescription;
  }

  public void setSettingDescription(String settingDescription) {
    this.settingDescription = settingDescription;
  }

  public SettingDefinition isTenantSpecific(Boolean isTenantSpecific) {
    this.isTenantSpecific = isTenantSpecific;
    return this;
  }

  /**
   * Whether this setting is present only for the current tenant, ie - hidden from other tenants.
   * @return isTenantSpecific
  */
  @ApiModelProperty(value = "Whether this setting is present only for the current tenant, ie - hidden from other tenants.")


  public Boolean getIsTenantSpecific() {
    return isTenantSpecific;
  }

  public void setIsTenantSpecific(Boolean isTenantSpecific) {
    this.isTenantSpecific = isTenantSpecific;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SettingDefinition settingDefinition = (SettingDefinition) o;
    return Objects.equals(this.settingName, settingDefinition.settingName) &&
        Objects.equals(this.isSecret, settingDefinition.isSecret) &&
        Objects.equals(this.settingDescription, settingDefinition.settingDescription) &&
        Objects.equals(this.isTenantSpecific, settingDefinition.isTenantSpecific);
  }

  @Override
  public int hashCode() {
    return Objects.hash(settingName, isSecret, settingDescription, isTenantSpecific);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SettingDefinition {\n");
    
    sb.append("    settingName: ").append(toIndentedString(settingName)).append("\n");
    sb.append("    isSecret: ").append(toIndentedString(isSecret)).append("\n");
    sb.append("    settingDescription: ").append(toIndentedString(settingDescription)).append("\n");
    sb.append("    isTenantSpecific: ").append(toIndentedString(isTenantSpecific)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

