package com.guidewire.jflightnova.client.helios.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * the operation to perform on a setting value within the batch request.
 */
@ApiModel(description = "the operation to perform on a setting value within the batch request.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-12-07T08:31:53.844517-08:00[America/Los_Angeles]")

public class BatchSettingValueRequestOld   {
  /**
   * Gets or Sets operation
   */
  public enum OperationEnum {
    CREATE("create"),
    
    UPDATE("update"),
    
    DELETE("delete");

    private String value;

    OperationEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static OperationEnum fromValue(String value) {
      for (OperationEnum b : OperationEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("operation")
  private OperationEnum operation;

  @JsonProperty("key")
  private String key;

  @JsonProperty("value")
  private String value;

  public BatchSettingValueRequestOld operation(OperationEnum operation) {
    this.operation = operation;
    return this;
  }

  /**
   * Get operation
   * @return operation
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public OperationEnum getOperation() {
    return operation;
  }

  public void setOperation(OperationEnum operation) {
    this.operation = operation;
  }

  public BatchSettingValueRequestOld key(String key) {
    this.key = key;
    return this;
  }

  /**
   * the name of the setting.
   * @return key
  */
  @ApiModelProperty(required = true, value = "the name of the setting.")
  @NotNull

@Pattern(regexp="^(\\w|\\.|\\-)*$") 
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public BatchSettingValueRequestOld value(String value) {
    this.value = value;
    return this;
  }

  /**
   * the value of the setting.
   * @return value
  */
  @ApiModelProperty(required = true, value = "the value of the setting.")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchSettingValueRequestOld batchSettingValueRequestOld = (BatchSettingValueRequestOld) o;
    return Objects.equals(this.operation, batchSettingValueRequestOld.operation) &&
        Objects.equals(this.key, batchSettingValueRequestOld.key) &&
        Objects.equals(this.value, batchSettingValueRequestOld.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operation, key, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BatchSettingValueRequestOld {\n");
    
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

