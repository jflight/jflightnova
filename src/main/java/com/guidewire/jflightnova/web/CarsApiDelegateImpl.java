package com.guidewire.jflightnova.web;

import com.guidewire.commons.logging.aop.ApiCall;
import com.guidewire.jflightnova.web.api.CarsApiDelegate;
import com.guidewire.jflightnova.repository.CarRepository;
import com.guidewire.jflightnova.web.api.model.Car;
import com.guidewire.commons.exception.BadRequestAlertException;
import com.guidewire.commons.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CarsApiDelegateImpl implements CarsApiDelegate {

    private CarRepository carRepository;

    @Autowired
    CarsApiDelegateImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    // Code to translate Domain Model to Api Model
    private Car convertToApiCar(com.guidewire.jflightnova.domain.Car domainCar) {
        Car car = new Car();
        car.setId(domainCar.getId());
        car.setColor(domainCar.getColor());
        car.setName(domainCar.getName());

        return car;
    }

    // Code to translate Api Model to Domain Model
    private com.guidewire.jflightnova.domain.Car convertToDomainCar(Car apiCar) {
        com.guidewire.jflightnova.domain.Car car = new com.guidewire.jflightnova.domain.Car();
        car.setId(apiCar.getId());
        car.setColor(apiCar.getColor());
        car.setName(apiCar.getName());

        return car;
    }

    @Override
    @ApiCall
    public ResponseEntity<Car> createCar(Car car) {
        if (car.getId() != null) {
            throw new BadRequestAlertException("A new car cannot already have an ID", "car", "idexists");
        }
        com.guidewire.jflightnova.domain.Car domainCar = convertToDomainCar(car);
        domainCar = carRepository.save(domainCar);
        return ResponseEntity.status(HttpStatus.CREATED).body(convertToApiCar(domainCar));
    }

    @Override
    @ApiCall
    public ResponseEntity<Void> deleteCar(Long id) {
        getCar(id);
        carRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    @ApiCall
    public ResponseEntity<List<Car>> getAllCars() {
        List<com.guidewire.jflightnova.domain.Car> domainCars = carRepository.findAll();
        List<Car> cars = domainCars.stream()
            .map(this::convertToApiCar).collect(Collectors.toList());
        return ResponseEntity.ok(cars);
    }

    @Override
    @ApiCall
    public ResponseEntity<Car> getCar(Long id) {
        Optional<com.guidewire.jflightnova.domain.Car> domainCar = carRepository.findById(id);
        if (!domainCar.isPresent()) {
            throw new EntityNotFoundException("Entity does not exist.", "car", "entitynotfound");
        }
        return ResponseEntity.ok(convertToApiCar(domainCar.get()));
    }

    @Override
    @ApiCall
    public ResponseEntity<Car> updateCar(Long id, Car car) {
        if (null == car.getId()) {
            car.setId(id);
        }
        if (!id.equals(car.getId())) {
            throw new BadRequestAlertException("ID provided in url param and in Car object do not match", "car", "idmismatch");
        }
        getCar(id);

        com.guidewire.jflightnova.domain.Car domainCar = convertToDomainCar(car);
        domainCar = carRepository.save(domainCar);
        return ResponseEntity.ok(convertToApiCar(domainCar));
    }
}
