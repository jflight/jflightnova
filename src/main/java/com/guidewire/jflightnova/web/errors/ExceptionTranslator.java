package com.guidewire.jflightnova.web.errors;

import com.guidewire.commons.exception.BaseExceptionTranslator;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
* You can place your custom exception handlers in this class.
 */
@ControllerAdvice
public class ExceptionTranslator extends BaseExceptionTranslator {
}
