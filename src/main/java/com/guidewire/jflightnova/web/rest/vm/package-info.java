/**
 * View Models used by Spring MVC REST controllers.
 */
package com.guidewire.jflightnova.web.rest.vm;
