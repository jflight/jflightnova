package com.guidewire.jflightnova.web;

import com.guidewire.commons.logging.aop.ApiCall;
import com.guidewire.jflightnova.web.api.ClientDecConfigsApiDelegate;
import com.guidewire.jflightnova.repository.ClientDecConfigRepository;
import com.guidewire.jflightnova.web.api.model.ClientDecConfig;
import com.guidewire.commons.exception.BadRequestAlertException;
import com.guidewire.commons.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClientDecConfigsApiDelegateImpl implements ClientDecConfigsApiDelegate {

    private ClientDecConfigRepository clientDecConfigRepository;

    @Autowired
    ClientDecConfigsApiDelegateImpl(ClientDecConfigRepository clientDecConfigRepository) {
        this.clientDecConfigRepository = clientDecConfigRepository;
    }

    // Code to translate Domain Model to Api Model
    private ClientDecConfig convertToApiClientDecConfig(com.guidewire.jflightnova.domain.ClientDecConfig domainClientDecConfig) {
        ClientDecConfig clientDecConfig = new ClientDecConfig();
        clientDecConfig.setId(domainClientDecConfig.getId());
        clientDecConfig.setCbsclientname(domainClientDecConfig.getCbsclientname());
        clientDecConfig.setEnvironmentLevel(domainClientDecConfig.getEnvironmentLevel());
        clientDecConfig.setDbUser(domainClientDecConfig.getDbUser());

        return clientDecConfig;
    }

    // Code to translate Api Model to Domain Model
    private com.guidewire.jflightnova.domain.ClientDecConfig convertToDomainClientDecConfig(ClientDecConfig apiClientDecConfig) {
        com.guidewire.jflightnova.domain.ClientDecConfig clientDecConfig = new com.guidewire.jflightnova.domain.ClientDecConfig();
        clientDecConfig.setId(apiClientDecConfig.getId());
        clientDecConfig.setCbsclientname(apiClientDecConfig.getCbsclientname());
        clientDecConfig.setEnvironmentLevel(apiClientDecConfig.getEnvironmentLevel());
        clientDecConfig.setDbUser(apiClientDecConfig.getDbUser());

        return clientDecConfig;
    }

    @Override
    @ApiCall
    public ResponseEntity<ClientDecConfig> createClientDecConfig(ClientDecConfig clientDecConfig) {
        if (clientDecConfig.getId() != null) {
            throw new BadRequestAlertException("A new clientDecConfig cannot already have an ID", "clientDecConfig", "idexists");
        }
        com.guidewire.jflightnova.domain.ClientDecConfig domainClientDecConfig = convertToDomainClientDecConfig(clientDecConfig);
        domainClientDecConfig = clientDecConfigRepository.save(domainClientDecConfig);
        return ResponseEntity.status(HttpStatus.CREATED).body(convertToApiClientDecConfig(domainClientDecConfig));
    }

    @Override
    @ApiCall
    public ResponseEntity<Void> deleteClientDecConfig(Long id) {
        getClientDecConfig(id);
        clientDecConfigRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    @ApiCall
    public ResponseEntity<List<ClientDecConfig>> getAllClientDecConfigs() {
        List<com.guidewire.jflightnova.domain.ClientDecConfig> domainClientDecConfigs = clientDecConfigRepository.findAll();
        List<ClientDecConfig> clientDecConfigs = domainClientDecConfigs.stream()
            .map(this::convertToApiClientDecConfig).collect(Collectors.toList());
        return ResponseEntity.ok(clientDecConfigs);
    }

    @Override
    @ApiCall
    public ResponseEntity<ClientDecConfig> getClientDecConfig(Long id) {
        Optional<com.guidewire.jflightnova.domain.ClientDecConfig> domainClientDecConfig = clientDecConfigRepository.findById(id);
        if (!domainClientDecConfig.isPresent()) {
            throw new EntityNotFoundException("Entity does not exist.", "clientDecConfig", "entitynotfound");
        }
        return ResponseEntity.ok(convertToApiClientDecConfig(domainClientDecConfig.get()));
    }

    @Override
    @ApiCall
    public ResponseEntity<ClientDecConfig> updateClientDecConfig(Long id, ClientDecConfig clientDecConfig) {
        if (null == clientDecConfig.getId()) {
            clientDecConfig.setId(id);
        }
        if (!id.equals(clientDecConfig.getId())) {
            throw new BadRequestAlertException("ID provided in url param and in ClientDecConfig object do not match", "clientDecConfig", "idmismatch");
        }
        getClientDecConfig(id);

        com.guidewire.jflightnova.domain.ClientDecConfig domainClientDecConfig = convertToDomainClientDecConfig(clientDecConfig);
        domainClientDecConfig = clientDecConfigRepository.save(domainClientDecConfig);
        return ResponseEntity.ok(convertToApiClientDecConfig(domainClientDecConfig));
    }
}
