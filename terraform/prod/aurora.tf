data "aws_secretsmanager_secret_version" "db_username" {
  secret_id = "tenant-wakanda-jflightnova/DATABASE_USERNAME"
}

data "aws_secretsmanager_secret_version" "db_password" {
  secret_id = "tenant-wakanda-jflightnova/DATABASE_PASSWORD"
}

module "aurora_db" {
  source = "git::ssh://git@stash.guidewire.com/ccs/atmos-tfmodule-aurora.git?ref=v0.14.2"

  name = "tenant-wakanda-jflightnova-aurora-${var.deploy_env}"

  atmos_cluster_name = "atmos-${var.deploy_env}"
  db_subnet_group_name = "atmos-${var.deploy_env}-rds-nonk8s-subnet-group"
  engine = "aurora-postgresql"
  engine_version = "10.11"
  instance_type = "db.r4.large"

  database_name = "jflightnova"
  username      = jsondecode(data.aws_secretsmanager_secret_version.db_username.secret_string)["DATABASE_USERNAME"]
  password      = jsondecode(data.aws_secretsmanager_secret_version.db_password.secret_string)["DATABASE_PASSWORD"]
  deletion_protection = true
}
