output "aurora_cluster_endpoint" {
  value = "${module.aurora_db.aurora_cluster_endpoint}"
}