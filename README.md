# jflightnova

- [Running the Application](#running-the-application)
- [Testing](#testing)
- [Logging](#logging)
- [Exception Handling](#exception-handling)
- [Authentication & Authorization](#authentication-&-authorization)
- [API-First Development](#api-first-development)

## Running the Application

#### Setting up Artifactory credentials if running locally

In order to pull gradle dependencies from Artifactory, you will need to set up your credentials. And there are two ways to do it:

> WARNING: For security considerations, we advise you to use your encrypted password. Get this by logging into
> [Artifactory](https://artifactory.guidewire.com/) and entering your password on the User Profile page.

1. Store your Artifactory username and encrypted password in your `~/.gradle/gradle.properties` file:
   ```shell script
   artifactoryUsername=<yourArtifactoryUsername>
   artifactoryPassword=<yourEncryptedArtifactoryPassword>
   ```
2. Set them as environment variables:
   ```shell script
   export ARTIFACTORY_USERNAME=<yourArtifactoryUsername>
   export ARTIFACTORY_PASSWORD=<yourEncryptedArtifactoryPassword>
   ```

#### Setting up Client Secret if running locally

The environment variable `CCA_CLIENT_SECRET` must be set for the server to start successfully.
See [Authentication & Authorization](#authentication-&-authorization)


#### Start the application

Run the application with `./gradlew`

By default the project runs with the "dev" spring profile (see `build.gradle` and the profile-specific `profile_[dev|prod].gradle`).

In order to run the service with the production profile, use `./gradlew -Pprod`.

## Testing

This application includes:

1. Unit tests for a few utilities (e.g `WebConfigurerTest.java`)
2. Integration tests for Controllers/Resources, Services, and Repositories.
3. Test Configuration (e.g. `application.yml`)

Unit tests can be run with `./gradlew test` and integration tests with `./gradlew integrationTest`

## Logging

This application uses the `com.guidewire.commons:logging` library, built on top of SL4J. This library requires developers to write their own logging statements, as opposed to AOP-style logging.

## Authentication & Authorization
This application has been provisioned with OAuth2 & OIDC via Cloud Common Auth.

The application requires the `CCA_CLIENT_SECRET` environment variable to be set when running locally.

To make calls to a protected endpoint, a token needs to be retrieved like so:
```bash
CLIENT_BASE64=$(echo -n 'CCA_CLIENT_ID:CCA_CLIENT_SECRET' | base64)
SCOPE='myapp:vendor' # Example scope
curl -X POST -s
    https://guidewire-hub.oktapreview.com/oauth2/ausj9ftnbxOqfGU4U0h7/v1/token
    -H 'Accept: application/json;charset=UTF-8'
    -H 'Authorization: Basic ${CLIENT_BASE64}'
    -H 'Content-Type: application/x-www-form-urlencoded'
    -d 'grant_type=client_credentials&scope=${SCOPE}'
```
`CCA_CLIENT_SECRET` can be retrieved from the AWS secret manager with name `CCA_CLIENT_SECRET`. `CLIENT_ID` can be found in `application.yml`.

### [Auth FAQ](https://nova.internal.guidewire.net/docs/dev/help-faq/#i-specified-scopes-when-i-ran-nova-auth-but-how-do-i-protect-my-resources-to-serve-only-if-the-token-had-that-scope)
## API-First Development

To implement API-First, an OpenAPI (Swagger) specification in `src/main/resources/swagger/api.yml` needs to be created.

Then `./gradlew openApiGenerate` can be run to generate endpoint interfaces under `build/generated-sources/openapi/src/main/java/package_name/web/api/`. Subsequently, "Delegate" interfaces needs to be implemented. An example:

```java
@Service
public class PetApiDelegateImpl implements PetApiDelegate {

  @Override
  public ResponseEntity<List<Pet>> findPetsByStatus(List<String> status) {
    return ResponseEntity.ok(
      status
        .stream()
        .distinct()
        .map(Pet.StatusEnum::fromValue)
        .map(statusEnum -> new Pet().id(RandomUtils.nextLong()).status(statusEnum))
        .collect(Collectors.toList())
    );
  }
}
```

## Additional Links

[Nova-Users Slack Channel](https://guidewire.slack.com/messages/nova-users/)

[Nova User Guide](https://nova.internal.guidewire.net/)

[JHipster Documentation](https://www.jhipster.tech)
